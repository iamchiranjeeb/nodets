import boto3,json

def deleteblog_posts_objects(posts:list):

    with open('./config.json') as rd:
        data = json.load(rd)

    try:

        s3 = boto3.client('s3')
        
        for post in posts:
            bucket = post['bucket']
            key = post['key']

            s3.delete_object(Bucket=bucket, Key=key)

            print(f"Object {key} deleted from bucket {bucket}")

    except Exception as e:
        print(e)


def delete_sqs_msg(receiptHandle:str):

    with open('./config.json') as rd:
        data = json.load(rd)

    QUEUE_URL = data['SQS_URL']

    try:

        sqs = boto3.client('sqs')
        sqs.delete_message(QueueUrl=QUEUE_URL, ReceiptHandle=receiptHandle)

    except Exception as e:
        print(e)