import json
from aws import deleteblog_posts_objects,delete_sqs_msg

def lambda_handler(event, context):
    
    records = event['Records']
    print('Total no of records received : {}'.format(len(records)))
    receipt_handle = records[0]['receiptHandle']

    for record in records:
        
        body = json.loads(record['body'])

        json_body = body
        type_task = json_body['type']

        match type_task:
            
            case "delete_posts":
                task_list = json_body['post_lists']
                deleteblog_posts_objects(task_list)
            case default:
                print('Type {} doesnot found'.format(type_task))

    delete_sqs_msg(receipt_handle)
