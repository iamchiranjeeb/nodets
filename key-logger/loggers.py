import logging
import os
from times import TIME
import time
from logging.handlers import TimedRotatingFileHandler,RotatingFileHandler

def getCurrentTime():
    return str(int(time.time()))


minute = TIME().Hours().split("-")[1]


def init_logger(logFilePath):
    logger = logging.getLogger("Key-Logs")
    logger.setLevel(logging.INFO)

    format = logging.Formatter("[%(name)s] : [%(asctime)s] : [%(levelname)s] : [%(filename)s:%(funcName)s:%(lineno)d]$ %(message)s")

    if not os.path.exists(logFilePath):
        os.mkdir(logFilePath)

    logger.handlers.clear()
    # fileHandler = logging.FileHandler(logFilePath + "/key-logger-" + minute +".log")
    fileHandler = TimedRotatingFileHandler(logFilePath + "/key-logger.log",when="m",interval=1)

    # streamHandler = logging.StreamHandler()

    fileHandler.setFormatter(format)
    # streamHandler.setFormatter(format)

    logger.addHandler(fileHandler)
    # logger.addHandler(streamHandler)

    return logger

def finalLineLogger(logFilePath):

    logger = logging.getLogger("Key-Logs-Final")
    logger.setLevel(logging.INFO)

    format = logging.Formatter("[%(name)s] : [%(asctime)s] : [%(levelname)s] : [%(filename)s:%(funcName)s:%(lineno)d]$ %(message)s")

    if not os.path.exists(logFilePath):
        os.mkdir(logFilePath)

    logger.handlers.clear()
    fileHandler = RotatingFileHandler(logFilePath + "/key-logger-full-line.log",maxBytes=20)

    # streamHandler = logging.StreamHandler()

    fileHandler.setFormatter(format)
    # streamHandler.setFormatter(format)

    logger.addHandler(fileHandler)
    # logger.addHandler(streamHandler)

    return logger



class SingletonType(type):
    _instances = {} ### protected variable

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(SingletonType, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Logger(object):

    def __init__(self,loggerDirectory):
        self._logger = init_logger(loggerDirectory)
        self._finallogger = finalLineLogger(loggerDirectory)
    
    def keyLogs(self):
        return self._logger

    def finalLogs(self):
        return self._finallogger