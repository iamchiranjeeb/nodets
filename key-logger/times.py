from datetime import datetime

class TIME(object):

    def __init__(self):
        self.now = datetime.now()

    def Years(self):
        self.years = str(self.now.day) + "/" + str(self.now.month) + "/" + str(self.now.year)
        return self.years

    def Hours(self):

        self.hours =str(self.now.hour) + "-" + str(self.now.minute) + "-" + str(self.now.second)
        return self.hours

    def FullTime(self):
        return self.Years() + " | " + self.Hours()
