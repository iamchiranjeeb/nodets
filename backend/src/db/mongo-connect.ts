import mongoose from 'mongoose';
// import config from 'config';
import config from '../config/default';
import fs from 'fs';
import path from 'path'



async function connect(){
    const mongoUri = config.mongoUri

    try{
        // const templatePath = path.join(__dirname,'../../');
        // var certFileBuf = fs.readFileSync(`${templatePath}/mongo-cert.pem`);
        

        // await mongoose.connect(mongoUri,{
        //     ssl:true,
        //     sslValidate: true,
        //     sslCA: `${templatePath}/mongo-cert.pem`,
        //     authMechanism: 'MONGODB-X509'
        // });X509-cert-8481638505079837929.pem
        console.log(mongoUri);
        mongoose.set('strictQuery', true);
        await mongoose.connect(mongoUri);
        // console.log("Current directory:", certFileBuf);
        console.log("Connected to database...")
    }catch(e){
        console.log("Error connecting to database..");
        console.log(e)
        console.log("Please try again")
        process.exit(1);
    }
}

export default connect;