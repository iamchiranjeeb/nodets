import * as redis from 'redis';
// import config from 'config';
import config from '../config/default';


// const redisURL = config.get<string>('reddisUri')
const redisURL = config.reddisUri

const client = redis.createClient(
    {url:redisURL}
);


export default client;