export default{
    port: 3001,
    host: '127.0.0.1',
    dbUri: "mongodb://127.0.0.1:27017/rest-test-ts2",
    JWT_SECRET: "iamchiranjeebsahoo"
};