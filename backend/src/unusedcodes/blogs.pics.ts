import multer, { FileFilterCallback } from 'multer';
import config from '../config/default';
import path from 'path';
import RequestWithUser from '../interfaces/requestWithUser.interface'
import fs from 'fs';
import fse from 'fs-extra';
import aws,{S3} from 'aws-sdk';
import { S3Client } from '@aws-sdk/client-s3';
import multerS3 from 'multer-s3';
import {Express,Request,Response,NextFunction} from 'express';

// aws.config.update({
//   secretAccessKey:config.AWS_CRED.SECRET_ACCESS_KEY,
//   accessKeyId:config.AWS_CRED.ACCESS_KEY_ID,
//   region:config.AWS_CRED.REGION
// })


const s3Config = new S3Client({
  region:config.AWS_CRED.REGION,
  credentials:{
    accessKeyId:config.AWS_CRED.ACCESS_KEY_ID,
    secretAccessKey:config.AWS_CRED.SECRET_ACCESS_KEY,
  }
})





const fileFilter = (req: Express.Request, file: Express.Multer.File, cb: FileFilterCallback) => {
   
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
      cb(null, true);
    } else {
      cb(new Error());
    }
};


function createDirIfNotExists(dirFullPath: string){
    if (fs.existsSync(dirFullPath)) {
        console.log('Directory exists!')
      } else {
        fs.mkdir(dirFullPath, { recursive: true }, err => {
            if (err) {
              throw err
            }
            console.log('Directory is created.')
          })
        console.log('Directory not found.')
    }
}


const uploadBlogs = multer({
    storage: multer.diskStorage({
        destination:function(req,file,cb){
            const fullDirPath = `./uploads/posts/${(req as RequestWithUser).user.id}/blogs/`;
            fse.mkdirsSync(fullDirPath);
            cb(null,fullDirPath);
        },
        filename:function (req,file,cb){
            return cb(null,`${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`);
        }
    }),
})

const uploadBlogsS3 = multer({

  storage:multerS3({
    s3:s3Config,
    bucket:config.AWS_CRED.BUCKET_NAME,
    acl:"public-read",
    metadata:function (req,file,cb){
      cb(null,{fieldname:"BlogsDoc"})
    },
    key: function (req,file,cb){
      cb(null,`${(req as RequestWithUser).user.id}/${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`)
    }
  })

}).array("file",10)


export default uploadBlogsS3;
  
