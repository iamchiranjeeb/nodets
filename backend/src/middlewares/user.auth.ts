import jsonwebtoken from 'jsonwebtoken';
import {Request,Response,NextFunction} from 'express';
import UserModel from '../models/user.model';
import {DataStoredInToken} from '../interfaces/user.interface';
import RequestWithUser from '../interfaces/requestWithUser.interface';
// import config from 'config';
import config from '../config/default';


function validateToken(token:any) {
    console.log('token type..')
    console.log(typeof token)
    if(typeof token === 'string') {
        return token;
    }
    return false;
}

export async function Auth(req: Request, res: Response, next: NextFunction){
    try {
        const JWT_SECRET = config.JWT_SECRET
        const authHeader = req.headers["authorization"];
        const AuthToken = authHeader && authHeader.split(" ")[1]

        if(AuthToken===null){
            return res.status(401).json({message:"Unauthorized Entry.....",success:false});
        }
        const isValidToken = validateToken(AuthToken);

        if(!isValidToken){
            return res.status(401).json({message:"Unauthorized Entry.)",success:false});
        }

        const decode = jsonwebtoken.verify(isValidToken,JWT_SECRET)
    
        const ifUserExists = await UserModel.findOne({ _id: decode });

        if(!ifUserExists){
            return res.status(401).json({message:"Unauthorized Entry.",success:false});
        }

        ifUserExists.isLoggedIn = true;
        ifUserExists.save();

        (req as RequestWithUser).user = ifUserExists;
        (req as RequestWithUser).token = isValidToken;

        next();	

        
    } catch (e:any) {
        res.status(401).json({message:e.message,success:false});
    }
}
