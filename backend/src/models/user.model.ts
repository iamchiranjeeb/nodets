import mongoose,{model,Model,Schema,Document} from 'mongoose';
import bcrypt from 'bcrypt';
import config from '../config/default';
import jsonwebtoken from 'jsonwebtoken';
import {UserDocument} from '../interfaces/user.interface';
import {deleteUsersS3Path} from '../services/user.service';
import Todos from './todos.model'
import Blogs from './blogs.model'
import reddiIO from '../utils/redis.chache';


const JWT_SECRET = config.JWT_SECRET


const userSchema = new Schema({
    email: {type:String,required:true,unique:true},
    name: {type:String,required:true},
    password: {type:String,required:true},
    isPrivateAccount: {type:Boolean,default:false,required:true},
    incomingFollowRequests: {type:Array,default:[]},
    totalIncomingFollowRequests: {type:Number,default:0},
    sentRequests: {type:Array,default:[]},
    totalSentRequests: {type:Number,default:0},
    followings: {type:Array,default:[]},
    followers: {type:Array,default:[]},
    totalfollwers: {type:Number,default:0},
    totalfollowings: {type:Number,default:0},
    isLoggedIn: {type: Boolean,default:false},
    tokens:[{
        token:{type:String}
    }]
},{
    timestamps:true
});

userSchema.methods.toJSON = function (){
    const user = this;
    const userObject = user.toObject();

    delete userObject.password
    delete userObject.tokens

    return userObject;
}


userSchema.methods.generateAuthToken = async function(){
    const user = this
    const token = jsonwebtoken.sign({_id: user._id.toSring()},JWT_SECRET)
    user.tokens = user.tokens.concat({token})
    await user.save()
    return token
}


userSchema.pre('save', async function(next){
    const user = this as UserDocument
    if(user.isModified('password')){
        user.password = await bcrypt.hash(user.password,10);
    }
    next()
})


// Delete User Todo When User is Removed
userSchema.pre('remove',async function(next) {
    const user = this as UserDocument
    await Todos.deleteMany({owner:user._id})
    await Blogs.deleteMany({owner:user._id})
    await deleteUsersS3Path(user._id)
    await reddiIO.deleteCacheUserObjects(user._id)
    next()
})

const UserModel:Model<UserDocument> = model<UserDocument>("User",userSchema);

export default UserModel;