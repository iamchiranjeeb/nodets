import mongoose,{model,Model,Schema,Document} from 'mongoose';
import {BlogDocument} from '../interfaces/blogs.interface';


const blogSchema = new Schema({
    header:{type:String,required:true},
    descrpition:{type:String},
    likedBy:{type:Array,default:[]},
    totalLikes: {type:Number,default:0},
    blogposts: {type:Array,default:[]},
    owner:{
        type:Schema.Types.ObjectId,
        required:true,
        ref:"User"
    }
},{
    timestamps: true
})


const Blogs:Model<BlogDocument> = model<BlogDocument>('blogs',blogSchema)

export default Blogs