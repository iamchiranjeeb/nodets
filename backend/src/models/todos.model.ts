import mongoose,{model,Model,Schema,Document} from 'mongoose';
import {TodoDocument} from '../interfaces/todos.interface';

const todoSchema = new Schema({
    todoName:{type:String,required:true},
    isDone:{type:Boolean,default:false},
    owner:{
        type:Schema.Types.ObjectId,
        required:true,
        ref:"User"
    }
},{
    timestamps: true
})


const Todos:Model<TodoDocument> = model<TodoDocument>('todos',todoSchema)

export default Todos