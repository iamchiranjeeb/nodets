import { createLogger, format } from 'winston';
// import {CloudWatchTransport} from 'winston-aws-cloudwatch';
import WinstonCloudWatch from 'winston-cloudwatch';
import aws from 'aws-sdk'
import config from '../config/default';

aws.config.update({
    accessKeyId: config.AWS_CRED.ACCESS_KEY_ID,
    secretAccessKey: config.AWS_CRED.SECRET_ACCESS_KEY,
    region: config.AWS_CRED.REGION, // Change this to your desired AWS region
  });


const log = createLogger({
    level:'debug',
    format: format.json(),
    transports:[

        new WinstonCloudWatch({
            level: 'debug',
            logGroupName: config.AWS_CRED.CLOUD_WATCH.LOG_GROUP, // REQUIRED
            logStreamName: 'node-application', // REQUIRED
            ensureLogGroup:true,
            jsonMessage:true,
            // awsRegion:config.AWS_CRED.REGION,
            // awsAccessKeyId:config.AWS_CRED.ACCESS_KEY_ID,
            // awsSecretKey:config.AWS_CRED.SECRET_ACCESS_KEY
            
        })
    
    ]
})

export default log