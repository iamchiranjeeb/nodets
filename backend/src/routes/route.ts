import {Express, Router} from 'express';
import {Auth} from '../middlewares/user.auth';
import config from '../config/default';
import Users from '../controller/user.controller';
import Todo from '../controller/todos.controller';
import Blog from '../controller/blogs.controller';
import BasicAuth from '../controller/getToken';
import uploadBlogs from '../utils/uploader';
import VideoPlayer from '../controller/videos.controllers';


class Routes{

    app:Express

    constructor(app:Express){
        this.app = app;
    }

    BasicAuthRoutes(){
        this.app.post('/api/get/authToken',BasicAuth.getAuth);
    }

    VideoTest(){
        this.app.get('/api/play/video/:videoFile',VideoPlayer.playTest);
    }

    Users(){
        this.app.get('/api/get/users',Users.viewUsers);
        this.app.post('/api/user',Users.createUserHandler);
        this.app.post('/api/login/user',Users.loginSingleUser);
        this.app.get('/api/me',Auth,Users.viewUsersProfile);
        this.app.post('/api/findUser',Auth,Users.searchUsers);
        this.app.post('/api/view/user/:userID',Auth,Users.viewUserProfile);
        this.app.patch('/api/credential/me/',Auth,Users.updateUserProfile);
        this.app.patch('/api/password/me/',Auth,Users.updateUserPassword);
        this.app.patch("/api/follow/:guestUserid",Auth,Users.followUser);
        this.app.patch("/api/unfollow/:guestUserid",Auth,Users.unfollowUser);
        this.app.patch("/api/change/accountType",Auth,Users.switchAccountBtnPrivatePublic);
        this.app.patch("/api/like/blog/:postId",Auth,Users.LikeAPost);
        this.app.patch("/api/response/requests/:incomingUserid",Auth,Users.AcceptOrRejectRequests);
        this.app.get("/api/view/followers",Auth,Users.viewFollowers);
        this.app.get("/api/view/followings",Auth,Users.viewFollowings);
        this.app.delete('/api/delete/me',Auth,Users.deleteSelfProfile);
        this.app.get('/api/logout/single/me',Auth,Users.logOutFromSingleApplication);
        this.app.get('/api/logout/multiple/me',Auth,Users.logOutFromMultipleApplication);
    }

    Todos(){
        this.app.post('/api/insert/todo',Auth,Todo.insertTodos);
        this.app.get('/api/view/todos',Auth,Todo.viewTodos);
        this.app.get('/api/view/notdones',Auth,Todo.notDoneTodos);
        this.app.get('/api/view/dones',Auth,Todo.DoneTodos);
        this.app.patch('/api/update/todo/:todo_id',Auth,Todo.updateTodos);
        this.app.delete('/api/delete/todo/:todo_id',Auth,Todo.deleteSingleTodo);
    }

    Blogs(){
        this.app.post('/api/create/blog',Auth,uploadBlogs.s3BlogUploader(config.AWS_CRED),Blog.createNewBlogPost);
        // this.app.post('/api/pic/blog',Auth,uploadBlogs.s3BlogUploader(config.AWS_CRED),Blog.addEditPhoto)
        this.app.get('/api/view/blogs',Auth,Blog.viewBlogs);
        this.app.patch('/api/update/blog/:blog_id',Auth,Blog.updateSingleBlog);
        this.app.delete('/api/delete/blog/:blog_id',Auth,Blog.deleteSingleBlog);
        this.app.get('/api/view/timeline/posts',Auth,Blog.viewTimelineposts)
    }

}

export default Routes