import {Express} from 'express';
import Users from '../controller/user.controller';

import {Auth} from '../middlewares/user.auth';

function routes(app:Express){
    app.get('/api/get/users',Users.viewUsers)
    app.post('/api/user',Users.createUserHandler);
    app.post('/api/login/user',Users.loginSingleUser);
    app.get('/api/me',Auth,Users.viewUsersProfile);
    app.patch('/api/credential/me/',Auth,Users.updateUserProfile);
    app.patch('/api/password/me/',Auth,Users.updateUserPassword)
    app.delete('/api/delete/me',Auth,Users.deleteSelfProfile);
    app.get('/api/logout/single/me',Auth,Users.logOutFromSingleApplication);
    app.get('/api/logout/multiple/me',Auth,Users.logOutFromMultipleApplication);
}

export default routes;