import {Express} from 'express';
import {Auth} from '../middlewares/user.auth';
import Todo from '../controller/todos.controller'

function todoRoutes(app: Express){
    app.post('/api/insert/todo',Auth,Todo.insertTodos);
    app.get('/api/view/todos',Auth,Todo.viewTodos);
    app.get('/api/view/notdones',Auth,Todo.notDoneTodos);
    app.get('/api/view/dones',Auth,Todo.DoneTodos);
    app.delete('/api/delete/todo/:todo_id',Auth,Todo.deleteSingleTodo);
}


export default todoRoutes;