import {Request,Response} from 'express';
import RequestWithUser from '../interfaces/requestWithUser.interface'
import UserModel from '../models/user.model';
import Blogs from '../models/blogs.model';
import reddiIO from '../utils/redis.chache';
import {Expires} from '../utils/user.enums';
import SQSObj from '../utils/Sqs';
import {removeElementFromArray} from '../services/user.service';
import {createBlog,viewaUserBlogs,patchBlog,DeleteOneBlog,getProperBlogList,filterFileObjects} from '../services/blogs.service';

class Blog {

    async createNewBlogPost(req: Request, res: Response){

        try{

            if(req.files){
                req.body["blogposts"] = req.files
            }

            const newBlog = createBlog({
                ...req.body,
                owner:(req as RequestWithUser).user.id
            })

            return res.status(200).json({success:true,message:newBlog})

        }catch(err:any){
            console.log(err);
            return res.status(409).json({success:false,message:err.message})
        }
    }

    async addEditPhoto(req: Request, res: Response){
        
        try{
            console.log(req.files)
            return res.status(200).json({success:true,message:"Files Uploaded"})
        }catch(err:any){
            console.log(err);
            return res.status(409).json({success:false,message:err.message})
        }
    }

    async viewBlogs(req: Request, res: Response){

        try{

            const user = (req as RequestWithUser).user;
            let blogs

            let usersBlog = await reddiIO.getFromReddis(`${user.id}-blogs`);

            if(usersBlog !== null && typeof usersBlog == "string"){
                blogs = JSON.parse(usersBlog);
                console.log("from reddis")
                return res.status(200).json({success:true,totalBlogs:blogs.length,blogs})
            }

            blogs = await viewaUserBlogs(user.id)
            // blogs.blogposts = await filterFileObjects(blogs.blogposts)
            console.log("adding to reddis")
            await reddiIO.insertToRedis(`${user.id}-blogs`,JSON.stringify(blogs),Expires.Yes,60);

            return res.status(200).json({success:true,totalBlogs:blogs.length,blogs})

        }catch(err:any){
            console.error('Error Occured.......................')
            console.log(err);
            return res.status(409).json({success:false,message:err.message})
        }
    }

    async updateSingleBlog(req: Request, res: Response){

        try{

            const {header,descrpition} = req.body;
            const blogId = req.params.blog_id;
            const ownerId = (req as RequestWithUser).user.id;

            if (!req.body) {
                return res.status(400).json({success:false,message:"Header && Description both are required."})
            }
            if (header === "" || header === null && descrpition === "" || descrpition === null){
                return res.status(400).json({success:false,message:"Todoname or Done status can nt be null."})
            }

            const patchSingleBlog = await patchBlog(blogId,ownerId,{...req.body})
            if (!patchSingleBlog) return res.status(404).json({success:false,message:"Could not find the Blogpost."})

            return res.status(200).json({success:true,message: patchSingleBlog})

        }catch(err:any){
            return res.status(500).json({success:false,message:err.message})
        }
    }

    async deleteSingleBlog(req: Request, res: Response){
        try{
            const userId = (req as RequestWithUser).user.id;
            const blogId = req.params.blog_id;
            const blog = await Blogs.findOne({_id: blogId});

            console.log('Delete blog post invoked...')

            if(!blog){
                return res.status(404).json({success:false,message:`Blog with id ${blogId} not found.`});
            }

            console.log('Blog.....')
            console.log(blog)

            const removeBlog = await DeleteOneBlog(userId,blogId);
            const sqs_msg = {
                "type":"delete_posts",
                "post_lists":blog.blogposts
            }
            SQSObj.sendMsg(JSON.stringify(sqs_msg))
            return res.status(200).json({success:true,message:removeBlog})

        }catch(err:any){
            return res.status(500).json({success:false,message:err.message})
        }
    }

    async viewTimelineposts(req: Request, res: Response){

        try {
            // console.log((req as RequestWithUser).user.followings)
            var userFollowings = (req as RequestWithUser).user.followings

            var resultant:any = [];

            for(var i=0; i<userFollowings.length; i++){

                let followings = await UserModel.findById(userFollowings[i]);

                if(!followings){
                    (req as RequestWithUser).user.followings = await removeElementFromArray((req as RequestWithUser).user.followings,userFollowings[i]);
                    // (req as RequestWithUser).user.totalfollowings = (req as RequestWithUser).user.totalfollowings - Number(1)
                    (req as RequestWithUser).user.save();
                }else{
                    // const blog = await Blogs.find({owner: userFollowings[i]});
                    const blog = await viewaUserBlogs(userFollowings[i])
                    resultant.push(blog);
                }

            }

            if(resultant.length > 0){
                resultant = await getProperBlogList(resultant)
            }

            return res.status(200).json({success:true,totalPosts:resultant.length,message:resultant})
        }catch(err:any){
            return res.status(500).json({success:false,message:err.message})
        }
    }
}


export default new Blog();