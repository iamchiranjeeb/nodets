import {Request,Response} from 'express';
import bcrypt from 'bcrypt';
import reddiIO from '../utils/redis.chache';
import UserModel from '../models/user.model';
import {createToken} from '../utils/GenerateToken';


class BasicAuth{

    async getAuth(req: Request, res: Response){

        try{
            const authorizaion = req.headers.authorization
            if(!authorizaion){
                return res.status(404).json({success:false,message:`Forbidden`});
            }

            const encode = authorizaion?.substring(6)
            const decode = Buffer.from(encode,'base64').toString('ascii')
            const [email,password] = decode.split(':')
            

            const user = await UserModel.findOne({email})
            if(!user) return res.status(404).json({success:false,message:`User with email ${email} not found`});
            
            const isMatchPassword = await bcrypt.compare(password, user.password)

            if(!isMatchPassword) return res.status(404).json({success:false,message:`Invalid Credentials`});;

            const token = await createToken(user)
            return res.status(200).json({success:true,token})

        } catch(e: any){
            return res.status(500).json({success:false,message:e.message})
        }

    }
}


export default new BasicAuth();