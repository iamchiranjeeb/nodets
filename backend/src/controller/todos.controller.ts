import {Request,Response} from 'express';
import RequestWithUser from '../interfaces/requestWithUser.interface'
import {
    createTodo,
    viewaUserTodos,
    viewNotDoneTodos,
    viewDoneTodos,
    DeleteaTodo,
    patchTodos
} from '../services/todos.service';
import UserModel from '../models/user.model';
import Todos from '../models/todos.model';


class Todo {

    async insertTodos(req: Request, res: Response){
        try {
    
            const todo = await createTodo({
                ...req.body,
                owner:(req as RequestWithUser).user.id
            })
            
            res.status(201).json({success:true,message:todo})
        } catch (e: any) {
            return res.status(409).json({success:false,message:e.message})
        }
    }
    
    async viewTodos(req: Request, res: Response) {
        try {
            const userId = (req as RequestWithUser).user.id
            const User = await UserModel.findOne({_id: userId});
    
            if (!User) {
                return res.status(400).json({success:false,message:"User not found"});
            }
    
            var todos = await viewaUserTodos(userId);
    
            res.status(200).json({success:true,totalTodos:todos.length,todos})
        } catch (e: any) {
            return res.status(500).json({success:false,message:e.message})
        }
    }
    
    
    async notDoneTodos(req: Request, res: Response){
        try {
            const userId = (req as RequestWithUser).user.id
            const User = await UserModel.findOne({_id: userId});
    
            if (!User) {
                return res.status(400).json({success:false,message:"User not found"});
            }
    
            var notDoneTodos = await viewNotDoneTodos(userId);
    
            res.status(200).json({success:true,totalNotTodos:notDoneTodos.length,notDoneTodos})
        } catch (e: any) {
            return res.status(500).json({success:false,message:e.message})
        }
    }
    
    async DoneTodos(req: Request, res: Response){
        try {
            const userId = (req as RequestWithUser).user.id
            const User = await UserModel.findOne({_id: userId});
    
            if (!User) {
                return res.status(400).json({success:false,message:"User not found"});
            }
    
            var notDoneTodos = await viewDoneTodos(userId);
    
            res.status(200).json({success:true,totalNotTodos:notDoneTodos.length,notDoneTodos})
        } catch (e: any) {
            return res.status(500).json({success:false,message:e.message})
        }
    }

    async updateTodos(req: Request, res: Response){
        try {

            const {todoName, isDone} = req.body;
            const todoId = req.params.todo_id;
            const ownerId = (req as RequestWithUser).user.id;

            if (!req.body) {
                return res.status(400).json({success:false,message:"Todoname && Done status both are required."})
            }

            if (todoName === "" || todoName === null && isDone === "" || isDone === null){
                return res.status(400).json({success:false,message:"Todoname or Done status can nt be null."})
            }

            const patchTodo = await patchTodos(todoId,ownerId,{...req.body})

            if (!patchTodo) return res.status(404).json({success:false,message:"Could not find the Todo."})

            return res.status(200).json({success:true,message: patchTodo})

        } catch (e: any) {
            return res.status(500).json({success:false,message:e.message})
        }
    }
    
    async deleteSingleTodo(req: Request, res: Response){
        try {
            const userId = (req as RequestWithUser).user.id
            const todoId = req.params.todo_id
            const User = await UserModel.findOne({_id: userId});
            const todo = await Todos.findOne({_id: todoId});
    
            if (!User) {
                return res.status(404).json({success:false,message:"User not found"});
            }
    
            if(!todo){
                return res.status(404).json({success:false,message:"Todo not found"});
            }
    
            const removeTodo = await DeleteaTodo(userId,todoId)
    
    
            res.status(200).json({success:true,message:removeTodo})
        } catch (e: any) {
            return res.status(500).json({success:false,message:e.message})
        }
    }
    
}

export default new Todo();