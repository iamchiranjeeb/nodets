import {Request,Response} from 'express';
import {
    createUser,
    viewAllUsers,
    loginUser,
    logoutAccount,
    logoutMultiDevice,
    updateData,
    findUser,
    checkMailExist,
    updatePassword,
    removeElementFromArray,
    viewUserData
} from '../services/user.service';
import UserModel from '../models/user.model';
import BlogModel from '../models/blogs.model';
import TodoModel from '../models/todos.model';
import {comparePasswords} from '../utils/userPasscheck';
import reddiIO from '../utils/redis.chache';
import {Expires} from '../utils/user.enums';
import RequestWithUser from '../interfaces/requestWithUser.interface';
import client from '../db/redis-connect';
import putLogger from '../utils/cloudwatch';

class Users {

    async createUserHandler(req: Request, res: Response){
        try {
    
            if(!comparePasswords(req.body.password, req.body.confirmPassword)){
                // await putLogger.putErrorlogs("User password mismatch")
                return res.status(400).json({success: false, message:"Password Not Matching"})
            }
    
            const createdUser = await createUser(req.body);
           
            if(!createdUser){
                return res.status(400).json({success:false,message:"User Exist"})
            }
    
            res.status(201).json({success:true,message:createdUser})
            // await putLogger.putDebuglogs(`User Created with ${req.body.email}`)
    
        } catch (e: any) {
            return res.status(409).json({success:false,message:e.message})
        }
    }

    async loginSingleUser(req: Request, res: Response){
        try {
    
            const userExists = await checkMailExist(req.body.email)
            
            if(!userExists) return res.status(404).json({success:false,message:"User Not Found"});
    
            const userLogin = await loginUser(req.body);
    
            if(!userLogin) {
                return res.status(404).json({success:false,message:"Invalid Credentials"});
            }

            console.log('..........................');
            console.log(userLogin[1])
            console.log(userLogin[0])
    
            res.status(200).json({success:true,message:"loggedin",user:userLogin[0],token:userLogin[1]});
        } catch (e: any) {
            return res.status(500).json({success:false,message:e.message})
        }
    }

    async viewUsersProfile(req: Request, res: Response){
        try {
            const profile = (req as RequestWithUser).user
            const token = (req as RequestWithUser).token

            console.log(profile._id)

            const blogCounts = await BlogModel.aggregate([

                {
                    $match: {
                      owner: profile._id
                    }
                },
                {
                    $group: {
                        _id: "$owner",
                        count: { $sum: 1 }
                    }
                }
                
            ])

            const usersTodos = await TodoModel.aggregate([

                {
                    $match: {
                      owner: profile._id
                    }
                },
                {
                    $facet:{
                        "trueCount": [
                            {
                                $match: {
                                    isDone: true
                                }
                            },
                            {
                                $group: {
                                    _id: null,
                                    count: { $sum: 1 }
                                }
                            }
                        ],
                        "falseCount": [
                            {
                                $match: {
                                    isDone: false
                                }
                            },
                            {
                                $group: {
                                    _id: null,
                                    count: { $sum: 1 }
                                }
                            }
                        ],
                        
                    }
                },
                // {
                //     $group: {
                //         _id: "$isDone",
                //         count: { $sum: 1 }
                //     }
                // },
                // {
                //     $group:{
                //         _id: null,
                //         total: { $sum: "$count" }
                //     }
                // }

            ])

            // console.log("Todos Count")
            // console.log(usersTodos)
            // profile.all_todos = usersTodos
            // console.log(profile)


            res.status(200).json({success:true,message:profile,Token:token})
        } catch (e: any) {
            console.log(e)
            return res.status(500).json({success:false,message:e.message})
        }
    }


    async updateUserProfile(req: Request, res: Response){
        try {
            const {email, name} = req.body;
    
            if(!email || !name) {
                return res.status(400).json({success:false,message:"Both email and name are required."})
            }
    
            const userExist = await findUser((req as RequestWithUser).user.id);
    
            if (!userExist) return res.status(404).json({success:false,message:"User Not Found."})
    
            const userUpdate = await updateData(req.body,(req as RequestWithUser).user.id);
    
            res.status(200).json({success:true,message:userUpdate})
            
        } catch (e: any) {
            return res.status(500).json({success:false,message:e.message})
        }
    }

    async updateUserPassword(req: Request, res: Response){
        try{
            const {password, confirmPassword} = req.body;
    
            const userExist = await findUser((req as RequestWithUser).user.id);
    
            if (!userExist) return res.status(404).json({success:false,message:"User Not Found."})
    
            if (!password || !confirmPassword) return res.status(400).json({success:false,message:"Both Password & Confirm Password Required"});
    
            if (!comparePasswords(password, confirmPassword)) return res.status(400).json({success: false, message:"Passwords Not Matching"});
    
            const passwordUpdate = await updatePassword(password,(req as RequestWithUser).user.id);
    
            res.status(200).json({success:true,message:passwordUpdate})
    
    
        } catch (e: any) {
            return res.status(500).json({success:false,message:e.message})
        }
        
    }

    async followUser(req: Request, res: Response){
        try {
            const guestUserID = req.params.guestUserid;

            const currentUser = (req as RequestWithUser).user;
            const findCurrentUser = await findUser(currentUser.id);
            if (!findCurrentUser) return res.status(404).json({success:false,message:"User Not Found."})

            var guestUser = await UserModel.findById(guestUserID)
            if (!guestUser) return res.status(404).json({success:false,message:"Guest User Not Found."})

            if(guestUserID !== currentUser.id){

                if(currentUser.followings.includes(guestUserID)){
                    return res.status(400).json({success:false,message:`You have already followed ${guestUser.email}.`})
                }
                else if(guestUser.isPrivateAccount && currentUser.followings.includes(guestUserID)===false){

                    if(guestUser.incomingFollowRequests.includes(currentUser.id) && currentUser.sentRequests.includes(guestUserID)){
                        return res.status(200).json({success:true,message:`Follow Request had already Sent to ${guestUser.id}`})
                    }

                    guestUser.incomingFollowRequests.push(currentUser.id);
                    guestUser.totalIncomingFollowRequests += 1
                    currentUser.sentRequests.push(guestUserID);
                    currentUser.totalSentRequests += 1

                    currentUser.save()
                    guestUser.save()

                    return res.status(200).json({success:true,message:`Follow Request Sent to ${guestUser.id}`})

                }else{

                    currentUser.followings.push(guestUserID)
                    currentUser.totalfollowings =  currentUser.totalfollowings+ Number(1)
                    currentUser.save()

                    if (guestUser !== null) {
                        guestUser.followers.push(currentUser.id)
                        guestUser.totalfollwers += Number(1)
                        guestUser.save()
                    }

                    return res.status(200).json({success:true,message:`You followed ${guestUser.email}`})
                }

            }else{
                return res.status(404).json({success:false,message:"You can't follow yourself."})
            }

        }catch(e: any) {
            return res.status(500).json({success:false,message:e.message})
        }
    }

    async AcceptOrRejectRequests(req: Request, res: Response){
        try{
            const currentUser = (req as RequestWithUser).user
            const incomingUserID = req.params.incomingUserid;
            const accept = req.body.accept;

            const findIncomingUser = await UserModel.findOne({_id: incomingUserID})
            if(!findIncomingUser){
                return res.status(404).json({success:false,message:`User with ID ${incomingUserID} Not Found.`})
            }
            if(currentUser.incomingFollowRequests.includes(incomingUserID)){

                if(accept){

                    currentUser.incomingFollowRequests = await removeElementFromArray(currentUser.incomingFollowRequests,incomingUserID)
                    currentUser.totalIncomingFollowRequests -= 1;
                    currentUser.followers.push(incomingUserID);
                    currentUser.totalfollwers += 1;
                    currentUser.save();

                    findIncomingUser.sentRequests = await removeElementFromArray(findIncomingUser.sentRequests,currentUser.id);
                    findIncomingUser.totalSentRequests -= 1;
                    findIncomingUser.followings.push(currentUser.id);
                    findIncomingUser.totalfollowings += 1;
                    findIncomingUser.save();

                    return res.status(200).json({success: true,message:`${findIncomingUser.id} follow request accepted.`})
                }else{
                    currentUser.incomingFollowRequests = await removeElementFromArray(currentUser.incomingFollowRequests,incomingUserID)
                    currentUser.totalIncomingFollowRequests -= 1;
                    currentUser.save();
                    findIncomingUser.sentRequests = await removeElementFromArray(findIncomingUser.sentRequests,currentUser.id);
                    findIncomingUser.totalSentRequests -= 1;
                    findIncomingUser.save();
                    return res.status(200).json({success: true,message:`${findIncomingUser.id} follow request rejected.`})
                }

            }else{
                return res.status(404).json({success:false,message:`User with ID ${incomingUserID} Not Found in Follow Requests.`})
            }

        }catch(e: any){
            return res.status(500).json({success:false,message:e.message})
        }
    }

    async unfollowUser(req: Request, res: Response){
        try{
            const guestUserID = req.params.guestUserid;
            const currentUser = (req as RequestWithUser).user;

            const findCurrentUser = await findUser(currentUser.id);
            if (!findCurrentUser) return res.status(404).json({success:false,message:"User Not Found."})

            var guestUser = await UserModel.findById(guestUserID)
            if (!guestUser) return res.status(404).json({success:false,message:`${guestUserID} User Not Found.`})

            if(guestUserID !== currentUser.id){

                if(currentUser.followings.includes(guestUserID)){
                    
                    currentUser.followings = await removeElementFromArray(currentUser.followings,guestUserID);
                    currentUser.totalfollowings -= Number(1);
                    await currentUser.save();

                    guestUser.followers = await removeElementFromArray(guestUser.followers,currentUser.id);
                    guestUser.totalfollwers -= Number(1);
                    await guestUser.save();

                    return res.status(200).json({success:true,message:`You unfollowed ${guestUser.email}`})

                }else{
                    return res.status(404).json({success:false,message:`You do nt follow ${guestUser.email}`})
                }

            }else{
                return res.status(404).json({success:false,message:"You can't unfollow yourself."})
            }

        }catch(e: any) {
            return res.status(500).json({success:false,message:e.message})
        }
    }

    async viewFollowers(req: Request, res: Response){
        try{

            const currentUser = (req as RequestWithUser).user;

            let resultantArray = [];

            for(var i=0; i<currentUser.followers.length; i++){

                let followers = await UserModel.findById(currentUser.followers[i]);

                if(!followers){
                    currentUser.followers = await removeElementFromArray(currentUser.followers,currentUser.followers[i]);
                }else{
                    var followersObj = {
                        "_id":followers._id,
                        "name":followers.name,
                        "email":followers.email,
                        "isLoggedIn":followers.isLoggedIn
                    }
                    resultantArray.push(followersObj);
                }
            }

            return res.status(200).json({success:true,message:`Follower Result for ${currentUser.email}.`,totalfollowers:resultantArray.length,followers:resultantArray})

        }catch(e: any) {
            return res.status(500).json({success:false,message:e.message})
        }
    }

    async viewFollowings(req: Request, res: Response){
        try{
            const currentUser = (req as RequestWithUser).user;

            let resultantArray = [];

            for(var i=0; i<currentUser.followings.length; i++){

                let followings = await UserModel.findById(currentUser.followings[i]);

                if(!followings){
                    currentUser.followings = await removeElementFromArray(currentUser.followings,currentUser.followings[i]);
                }else{
                    var followingsObj = {
                        "_id": currentUser.followings[i],
                        "name":followings.name,
                        "email":followings.email,
                        "isLoggedIn":followings.isLoggedIn
                    }
                    resultantArray.push(followingsObj);
                }

            }

            await reddiIO.insertToRedis(`${currentUser.id}-followings`,JSON.stringify(resultantArray),Expires.Yes,10);

            return res.status(200).json({success:true,message:`Following Result for ${currentUser.email}.`,totalfollowings:resultantArray.length,followings:resultantArray})

        }catch(e: any) {
            return res.status(500).json({success:false,message:e.message})
        }
    }

    async requestUserToFollow(req: Request, res: Response){

        try{
            const guestUserID = req.params.guestUserid;
            const currentUser = (req as RequestWithUser).user;

        }catch(e: any) {
            return res.status(500).json({success:false,message:e.message})
        }
    }

    async LikeAPost(req: Request, res: Response){

        const currentUser = (req as RequestWithUser).user;
        const reqBlogId = req.params.postId;

        try{

            const findRequestedBlog = await BlogModel.findOne({_id:reqBlogId})
            if(!findRequestedBlog){
                return res.status(404).json({success:false,message:`Blog With Id ${reqBlogId} Could not be found.`})
            }

            const currUserFollwings = currentUser.followings;
            const ifPostAlreadyLiked = findRequestedBlog.likedBy.includes(currentUser.id)
            
            var message = ""
            if(!ifPostAlreadyLiked){
                findRequestedBlog.likedBy.push(currentUser.id);
                findRequestedBlog.totalLikes += 1;
                message = `You Liked The Blog ${findRequestedBlog.header}`
            }else if(ifPostAlreadyLiked){
                findRequestedBlog.likedBy = await removeElementFromArray(findRequestedBlog.likedBy,currentUser.id);
                findRequestedBlog.totalLikes -= 1;
                message = `You Disliked The Blog ${findRequestedBlog.header}`
            }

            findRequestedBlog.save();
            return res.status(200).json({success:true,message})

        }catch(e: any) {
            return res.status(500).json({success:false,message:e.message})
        }
    }


    async switchAccountBtnPrivatePublic(req: Request, res: Response){
        const currentUser = (req as RequestWithUser).user;

        try{

            let initialStatus = currentUser.isPrivateAccount;

            if(initialStatus){
                currentUser.isPrivateAccount = false;
            }else if(!initialStatus){
                currentUser.isPrivateAccount = true;
            }

            currentUser.save();

            var message=""
            if(currentUser.isPrivateAccount === initialStatus){
                message = "Account Type Could not Be Changed."
            }else{
                if(currentUser.isPrivateAccount === true && initialStatus === false){
                    message = "Account Type Changed from Public to Private."
                }else if(currentUser.isPrivateAccount === false && initialStatus === true){
                    message = "Account Type Changed from Private to Public."
                }
            }

            return res.status(200).json({success:true,accountType:currentUser.isPrivateAccount,message})

        }catch(e: any) {
            return res.status(500).json({success:false,message:e.message})
        }
    }

    async deleteSelfProfile(req: Request, res: Response){
        try {
            const selfProfile = (req as RequestWithUser).user
            await selfProfile.remove()
            res.status(200).json({success:true,message:selfProfile})
        } catch (e: any) {
            return res.status(500).json({success:false,message:e.message})
        }
    }
    
    
    async logOutFromSingleApplication(req: Request, res: Response){
        try {
            const loggedOut = await logoutAccount(req as RequestWithUser)
            if (loggedOut) return res.status(200).json({success:true,message:"Logged Out."});
        } catch (e: any) {
            return res.status(500).json({success:false,message:e.message})
        }
    }
    
    async logOutFromMultipleApplication(req: Request, res: Response){
        try{
            const loggedOut = await logoutMultiDevice(req as RequestWithUser)
            if (loggedOut) return res.status(200).json({success:true,message:"Logged Out From all Devices."});
        }catch (e: any) {
            return res.status(500).json({success:false,message:e.message})
        }
    }

    async viewUsers(req: Request, res: Response){
        try {
            const showUsers = await viewAllUsers();
            return res.status(200).json({success:true,message:showUsers})
        } catch (e: any) {
            return res.status(500).json({success:false,message:e.message})
        }
    }

    async searchUsers(req: Request, res: Response){

        try{
            //
            const {user_string} = req.body;
            const selfProfile = (req as RequestWithUser).user

            // const result = await UserModel.find({name:new RegExp(user_string, 'i')})

            const result = await UserModel.find({

                $and:[
                    {name:new RegExp(user_string, 'i')},
                    { _id: { $ne: selfProfile.id } }
                ]

            })

            console.log("Here result is")
            console.log(result)

            return res.status(200).json({success:true,message:result})

        }catch(e:any){
            return res.status(500).json({success:false,message:e.message})
        }

    }

    async viewUserProfile(req: Request, res: Response){

        try{
            
            const guestUserID = req.params.userID;

            const foundUser = await UserModel.findOne({_id:guestUserID})

            if(!foundUser){
                return res.status(404).json({success:false,message:`User with ID ${guestUserID} not found`})
            }

            return res.status(200).json({success:true,message:foundUser})

        }catch(e:any){
            return res.status(500).json({success:false,message:e.message})
        }

    }
    
    
}

export default new Users();
