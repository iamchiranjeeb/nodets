import {Request,Response} from 'express';
import fs from 'fs';
import path from 'path';

class VideoPlayersTest{

    playTest(req: Request, res: Response){
        
        try {
            console.log("File Path : ")
            const currentDirectory = __dirname;
            const parentDirectory = path.join(currentDirectory, '../');
            console.log(parentDirectory)

            const videoMappers: { [key: string]: string } ={
                'cdn':'resources/TestSmart.mp4',
            }

            const videoFile = req.params.videoFile
            // const fullVideoPath = path.join(parentDirectory,)
            // const filePath = videoMappers[videoFile]

            const filePath = path.join(parentDirectory,videoMappers[videoFile])
            console.log("File Path Now : " + filePath)

            if(!filePath){
                return res.status(404).send('File not found')
            }

            const stat = fs.statSync(filePath);
            const fileSize = stat.size;
            const range = req.headers.range;


            if(range){
                const parts = range.replace(/bytes=/, '').split('-')
                const start = parseInt(parts[0], 10);
                const end = parts[1] ? parseInt(parts[1], 10) : fileSize - 1;

                const chunksize = end - start + 1;
                const file = fs.createReadStream(filePath, {start, end});
                const head = {
                    'Content-Range': `bytes ${start}-${end}/${fileSize}`,
                    'Accept-Ranges': 'bytes',
                    'Content-Length': chunksize,
                    'Content-Type': 'video/mp4'
                };

                res.writeHead(206, head);
                file.pipe(res);


            }else{
                
                const head = {
                    'Content-Length': fileSize,
                    'Content-Type': 'video/mp4'
                };
                res.writeHead(200, head);
                fs.createReadStream(filePath).pipe(res)

            }




            
        } catch (e: any) {
            console.error("Video Error")
            console.error(e)
            return res.status(500).json({success:false,message:e.message})
        }

    }

}



export default new VideoPlayersTest();