
export interface ReqSingleObject {
    originalname:string;
    size:number;
    bucket:string;
    key: string;
    contentType: string;
    location: string;
}

export interface s3SingleFileObject {
    fieldname: string | undefined,
    originalname: string | undefined,
    encoding: string | undefined,
    mimetype: string | undefined,
    size: number | undefined,
    bucket: string | undefined,
    key: string | undefined,
    acl: string | undefined | null,
    contentType: string | undefined,
    contentDisposition: any,
    contentEncoding: any,
    storageClass: string | undefined,
    serverSideEncryption: any,
    metadata: { fieldname: string | undefined } | any,
    location: string | undefined,
    etag: string | undefined | any,
    versionId: string | number | undefined
}

export interface ArrayOfSingleObjects {
    blogposts:s3SingleFileObject[] | [];
}

 