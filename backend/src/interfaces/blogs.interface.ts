import {Document} from 'mongoose';
import {ArrayOfSingleObjects} from './s3uploader.interface';


export interface BlogDocument extends ArrayOfSingleObjects,Document{
    _id: any;
    header: string;
    descrpition: string;
    likedBy:Array<string>;
    totalLikes: number;
    owner: any;
    createdAt: Date;
    updatedAt: Date;
}


export interface createBlogPost{
    header: string;
    descrpition: string;
    owner: any;
}

export interface updateBlogPost{
    header: string;
    descrpition: string;
}