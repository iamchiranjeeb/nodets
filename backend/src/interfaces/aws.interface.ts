
export interface AWSUploaderConfig{
    BUCKET_NAME : string;
    REGION : string;
    SECRET_ACCESS_KEY : string;
    ACCESS_KEY_ID : string;
}

interface CloudWatchGroup  {
    logGroupName: string,
    creationTime: string | number,
    metricFilterCount: number,
    arn: string,
    storedBytes: number
}

export interface CloudWatchGroups {
    logGroupsCloudWtach : CloudWatchGroup[] | any
}