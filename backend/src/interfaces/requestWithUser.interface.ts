import { Request } from 'express';
import {UserDocument} from './user.interface';

interface RequestWithUser extends Request {
    user:UserDocument;
    token: string;
}

export default RequestWithUser