import {Document} from 'mongoose';

export interface UserDocument extends Document{
    _id: any;
    email: string;
    name: string;
    password: string;
    isPrivateAccount:boolean;
    incomingFollowRequests: Array<string>;
    totalIncomingFollowRequests: number;
    sentRequests: Array<string>;
    totalSentRequests: number;
    followers: Array<string>;
    followings: Array<string>;
    totalfollwers: number,
    totalfollowings: number,
    confirmPassword: string;
    isLoggedIn:boolean;
    tokens:Array<Object>;
    createdAt: Date;
    updatedAt: Date;
}

export interface UserLoginDocument{
    email : string;
    password : string;
}

export interface DataStoredInToken {
    _id: string;
}

export interface userCredentials{
    email: string;
    name : string;
}

