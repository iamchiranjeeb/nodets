import {Document} from 'mongoose';

export interface TodoDocument extends Document{
    _id: any;
    todoName: string;
    isDone: boolean;
    owner: any;
    createdAt: Date;
    updatedAt: Date;
}

export interface createTodo{
    todoName: string;
    isDone: boolean;
    owner: any;
}

export interface updateTodo{
    todoName: string;
    isDone: boolean;
}