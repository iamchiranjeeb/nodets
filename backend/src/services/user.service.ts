import {DocumentDefinition} from 'mongoose';
import UserModel from '../models/user.model';
import {UserDocument,UserLoginDocument,userCredentials,DataStoredInToken} from '../interfaces/user.interface';
import RequestWithUser from '../interfaces/requestWithUser.interface'
import {createToken} from '../utils/GenerateToken';
import Path from '../utils/usefullpaths';
import bcrypt from 'bcrypt';
import config from '../config/default';
import awsConf from '../utils/aws.config';



export async function createUser(input:DocumentDefinition<Omit<UserDocument,'createdAt' | 'updatedAt'>>){

    try {
        const findUser = await UserModel.findOne({email:input.email})
    
        if(findUser){
            return false;
        }

        return await UserModel.create(input)
    } catch (e: any) {
        throw new Error(e);
    }
}



export async function loginUser(input:UserLoginDocument){
    try {
        
        const user = await UserModel.findOne({email:input.email})

        if(!user) return false;

        const isMatchPassword = await bcrypt.compare(input.password, user.password)
        
        if(!isMatchPassword) return false;

        const token = await createToken(user)

        return [user,token]
    } catch (e : any) {
        throw new Error(e);
    }
}


export async function updateData(input:userCredentials,id:string){
    try {

        const updateData = await UserModel.findByIdAndUpdate(id,{email:input.email,name:input.name},{new:true});

        return updateData

    } catch (e: any) {
        throw new Error(e);
    }
}

export async function updatePassword(input:string,id:string){
    try {

        const User = await UserModel.findOne({_id:id})

        if(User !== null){
            User.password = input
            await User.save()
        }

        return User;
        
    } catch (e: any) {
        throw new Error(e);
    }
}

export async function viewUserData(id:string){
    return await UserModel.findOne({_id:id})
}

export async function removeElementFromArray(array:Array<string>,n:string){
    const index = array.indexOf(n);

    // if the element is in the array, remove it
    if(index > -1){
        array.splice(index, 1);
    }

    return array;
}

export async function findUser(input:string){
    try {
        const foundUser = await UserModel.findOne({_id:input})

        if(!foundUser){return false}

        return true;

    } catch (e: any) {
        throw new Error(e);
    }
}

export async function checkMailExist(email: string) {
    try {
        const foundUser = await UserModel.findOne({email})

        if(!foundUser) return false

        return true;

    } catch (e: any) {
        throw new Error(e);
    }
}

export async function logoutAccount(req:RequestWithUser){
    try {
        req.user.tokens = req.user.tokens.filter((token:any) => {
            return token.token !== req.token
        })

        req.user.isLoggedIn = false;
        await req.user.save()

        return true

    } catch (e: any) {
        throw new Error(e);
    }
}

export async function logoutMultiDevice(req:RequestWithUser){
    try {
        req.user.tokens = []
        req.user.isLoggedIn = false;
        await req.user.save()
        return true
    } catch (e: any) {
        throw new Error(e);
    }
}

export async function viewAllUsers(){
    try {
        const users = await UserModel.find()

        return users

    } catch (e: any) {
        throw new Error(e);
    }
}

export async function deleteUsersS3Path(userId:any){
    await new awsConf(config.AWS_CRED.REGION,config.AWS_CRED.ACCESS_KEY_ID,config.AWS_CRED.SECRET_ACCESS_KEY).deleteS3Object(config.AWS_CRED.BUCKET_NAME,Path.getUsersBaseS3Path(userId))
}