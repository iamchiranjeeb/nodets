import {DocumentDefinition,Schema} from 'mongoose';
import {createTodo,updateTodo} from '../interfaces/todos.interface';
import Todos from '../models/todos.model';


export async function createTodo(input: createTodo){
    try {
        
        const todo = await Todos.create(input)
        
        return todo
    } catch (e: any) {
        throw new Error(e);
    }
}

export async function viewaUserTodos(id: string){
    try {
        const tasks = await Todos.find({owner: id});
        return tasks
    }catch(e: any) {
        throw new Error(e);
    }
}

export async function viewNotDoneTodos(id:string){
    try {
        const notDoneTodos = await Todos.find({owner: id,isDone:false})
        return notDoneTodos
    } catch (e: any) {
        throw new Error(e);
    }
}

export async function viewDoneTodos(id:string){
    try {
        const notDoneTodos = await Todos.find({owner: id,isDone:true})
        return notDoneTodos
    } catch (e: any) {
        throw new Error(e);
    }
}


export async function patchTodos(id: string,ownerId: string,input: updateTodo){
    try {

        const Todo = await Todos.findOne({_id: id,owner: ownerId})

        if (Todo !== null && Todo) {

            Todo.todoName = input.todoName;
            Todo.isDone = input.isDone;
            await Todo.save()

            return Todo;
        }

        return false;
        
    } catch (e: any) {
        throw new Error(e);
    }
}

export async function DeleteaTodo(userId:string,todoId:string){
    try {
        const todo = await Todos.deleteOne({_id:todoId})
        return todo
    } catch (e: any) {
        throw new Error(e);
    }
}