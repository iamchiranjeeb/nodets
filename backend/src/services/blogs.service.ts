import {createBlogPost,updateBlogPost} from '../interfaces/blogs.interface';
import {s3SingleFileObject} from '../interfaces/s3uploader.interface';
import Blog from '../models/blogs.model';


export async function createBlog(input:createBlogPost){
    try{
        const blog = await Blog.create(input);

        return blog;
    }catch(e: any){
        throw new Error(e);
    }
}

export function filterFileObjects(input:s3SingleFileObject[]){

    return input.filter(sobj => {
        delete sobj.fieldname
        delete sobj.encoding
        delete sobj.mimetype
        delete sobj.acl
        delete sobj.contentDisposition
        delete sobj.contentEncoding
        delete sobj.storageClass
        delete sobj.serverSideEncryption
        delete sobj.metadata
        delete sobj.etag
        delete sobj.versionId
        return sobj
    })

}

export async function viewaUserBlogs(id: string){
    try {
         var allBlogs = await Blog.find({owner: id});

         for(var i=0; i<allBlogs.length; i++){
            allBlogs[i].blogposts = filterFileObjects(allBlogs[i].blogposts)
         }
        
         return allBlogs
    }catch(e: any) {
        throw new Error(e);
    }
}

export async function patchBlog(id: string,ownerId: string,input: updateBlogPost){
    try{
        const SingleBlog = await Blog.findOne({_id: id,owner: ownerId})

        if(SingleBlog !== null && SingleBlog){
            SingleBlog.header = input.header;
            SingleBlog.descrpition = input.descrpition;
            await SingleBlog.save();
            return SingleBlog;
        }

        return false;

    }catch(e: any) {
        throw new Error(e);
    }
}

export async function DeleteOneBlog(userId:string,blogId:string){
    try {
        const blog = await Blog.deleteOne({_id:blogId,owner:userId});
        return blog;
    } catch (e: any) {
        throw new Error(e);
    }
}

export async function getProperBlogList(blogArray:Array<any>){
    try {
        blogArray = blogArray.flat()

        var resultant:any = []

        for(var i=0; i<blogArray.length; i++){

            var postsArray = [];
            if(blogArray[i].blogposts.length > 0){
                for(var j=0; j<blogArray[i].blogposts.length; j++){
                    postsArray.push(blogArray[i].blogposts[j].location)
                }
            }
            
            var singleObj = {
                "header":blogArray[i].header,
                "description":blogArray[i].descrpition,
                "posts":postsArray,
                "likedBy":blogArray[i].likedBy,
                "totalLikes":blogArray[i].totalLikes
            }
            resultant.push(singleObj);
        }


        return resultant.sort(() => Math.random() - 0.5);

    }catch (e: any) {
        throw new Error(e);
    }
}