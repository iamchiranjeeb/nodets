import express from 'express';
import connect from './db/mongo-connect';
import reddisClient from './db/redis-connect';
import config from './config/default';
import Routes from './routes/route';
import PutLogger from './utils/cloudwatch'
import aws from 'aws-sdk';
import cors from 'cors';

PutLogger.Initializer()

const app = express();

app.use(express.json());
app.use(cors());
connect();

reddisClient.on('connect', function() {
    console.log('Connected!');
});


reddisClient.on('error', (err) => console.log('Redis Client Error', err));

reddisClient.connect();


const routes = new Routes(app)
routes.Users()
routes.Todos()
routes.Blogs()
routes.BasicAuthRoutes()
routes.VideoTest()

export default app
