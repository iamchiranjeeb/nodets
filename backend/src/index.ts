import config from './config/default';
import app from './app';
import http from 'http'
import {Server} from 'socket.io';
import os from 'os';
import CPUStats from './utils/cpus.stats';

const PORT = config.port
const HOST = config.host

const server = http.createServer(app)


const io = new Server(server,{
    cors:{
        origin:"http://localhost:3066"
    }
})

const stats= new CPUStats()


io.on('connection',(socket)=>{

    console.log("Client Connected...")

    var msgObject = {
        "system_info":{
            "hostname":os.hostname(),
            "platform":os.platform(),
            "release":os.release(),
            "architecture":os.arch()
        }
    }


    const streamDatas = setInterval(() => {
        socket.emit('streamData',JSON.stringify(msgObject))
    },3000)


    var cpuInfo = {
        "CPU Usage":stats.calculateCpuUsage()
    }

    socket.on("cpuInfo",(data:any) => {

        socket.emit("cpuInfo",JSON.stringify(cpuInfo))

    })

    socket.on('memoryInfo',(data:any) => {
        const memoryInfo = stats.memoryUsageInfo()
        socket.emit("memoryInfo",JSON.stringify(memoryInfo))
    })


    socket.on('is_active',(data:any) => {
        socket.emit('is_active',true)
    })


})




server.listen(PORT,async()=>{
    console.log(`Server is up on http://${HOST}:${PORT}`);
})