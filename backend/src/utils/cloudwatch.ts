import aws from 'aws-sdk';
import awsConfigs from './aws.config';
import config from '../config/default';
require('aws-sdk/lib/maintenance_mode_message').suppress = true;

class CloudwatchService {

    private cloudWatchLogs:aws.CloudWatchLogs

    constructor(cloudWatchLogs:aws.CloudWatchLogs) {
        this.cloudWatchLogs = cloudWatchLogs;
    }

    getCloudWatchLogs():aws.CloudWatchLogs{
        return this.cloudWatchLogs
    }

    listLogGroups():Promise<aws.CloudWatchLogs.LogGroup[]>{

        return new Promise((resolve,reject) => {

            this.cloudWatchLogs.describeLogGroups({},(error,data)=>{

                if(error) {
                    reject(error);
                }else{
                    if(data.logGroups != undefined && data.logGroups.length > 0) {
                        resolve(data.logGroups);
                    }
                    resolve([]);
                }

            })

        })

    }

    listLogStrems(logGroup:string):Promise<aws.CloudWatchLogs.LogStream[]>{
        return new Promise((resolve, reject) => {
            this.cloudWatchLogs.describeLogStreams({logGroupName:logGroup},(error,data)=>{

                if(error) {
                    reject(error);
                }else{
                    if(data.logStreams != undefined && data.logStreams.length > 0) {
                        resolve(data.logStreams);
                    }
                    resolve([])
                }

            })

        })
    }

    createLogGroup(logGroupName:string){

        return new Promise((resolve, reject) => {

            this.cloudWatchLogs.createLogGroup({logGroupName},(err,data)=>{

                if(err) {
                    console.error("Error while creating cloud watch log group==>")
                    console.error(err);
                    reject(err);
                }else{
                    console.log("Log Group Created Successfully...")
                    console.log(data)
                    resolve(data)
                }

            })

        })

    }

    createLogStream(logGroupName:string,logStreamName:string){

        return new Promise((resolve, reject) => {

            this.cloudWatchLogs.createLogStream({logGroupName,logStreamName},(err, data) => {

                if(err) {
                    console.error("Error while creating LogStreams for LogGroup: " + logGroupName)
                    console.error(err);
                    reject(err);
                }else{
                    console.log("Log Stream Created " + logStreamName)
                    console.log(data)
                    resolve(data)
                }

            })

        })

    }

}

class PutLogs {

    private logGroupName = config.AWS_CRED.CLOUD_WATCH.LOG_GROUP
    private errorStreamName = config.AWS_CRED.CLOUD_WATCH.ERROR_STREAM
    private debugStream = config.AWS_CRED.CLOUD_WATCH.DEBUG_STREAM
    private CloudWatch:CloudwatchService;

    constructor(CloudWatch:CloudwatchService){

        console.log("Check if here ...")

        this.CloudWatch = CloudWatch

        // CloudWatch.listLogGroups().then((groups) => {
        //     const foundLogGroup = groups.find((element:aws.CloudWatchLogs.LogGroup)=> element.logGroupName === config.AWS_CRED.CLOUD_WATCH.LOG_GROUP)
        //     if(!foundLogGroup) {
        //         console.error("Configured log group not present")
        //         CloudWatch.createLogGroup(config.AWS_CRED.CLOUD_WATCH.LOG_GROUP)
        //     }
            
        // })
        // .catch(err => console.log(err));

        // CloudWatch.listLogStrems(config.AWS_CRED.CLOUD_WATCH.LOG_GROUP).then((streams) => {
        //     const foundErrorStream = streams.find((ele:aws.CloudWatchLogs.LogStream)=>ele.logStreamName === config.AWS_CRED.CLOUD_WATCH.ERROR_STREAM)
        //     const foundDebugStream = streams.find((ele:aws.CloudWatchLogs.LogStream)=>ele.logStreamName === config.AWS_CRED.CLOUD_WATCH.DEBUG_STREAM)
        
        //     if(!foundErrorStream) {
        //         CloudWatch.createLogStream(config.AWS_CRED.CLOUD_WATCH.LOG_GROUP,config.AWS_CRED.CLOUD_WATCH.ERROR_STREAM)
        //     }
        
        //     if(!foundDebugStream) {
        //         CloudWatch.createLogStream(config.AWS_CRED.CLOUD_WATCH.LOG_GROUP,config.AWS_CRED.CLOUD_WATCH.DEBUG_STREAM)
        //     }
        // })
        // .catch(err => console.log(err));


    }

    async Initializer () {
        try{
            //
            const logGroups = await this.CloudWatch.listLogGroups();
            const foundLogGroup = await logGroups.find((element:aws.CloudWatchLogs.LogGroup)=> element.logGroupName === this.logGroupName)
            if(!foundLogGroup) {
                console.error("Configured log group not present")
                await this.CloudWatch.createLogGroup(this.logGroupName)
            }
            //
            const logStreams = await this.CloudWatch.listLogStrems(this.logGroupName)

            const foundErrorStream = await logStreams.find((ele:aws.CloudWatchLogs.LogStream)=>ele.logStreamName === this.errorStreamName)
            const foundDebugStream = await logStreams.find((ele:aws.CloudWatchLogs.LogStream)=>ele.logStreamName === this.debugStream)

            if(!foundErrorStream) {
                await this.CloudWatch.createLogStream(this.logGroupName,this.errorStreamName)
            }
        
            if(!foundDebugStream) {
                await this.CloudWatch.createLogStream(this.logGroupName,this.debugStream)
            }

            console.log("Initializer Ended ...")

        }catch(err){
            console.error(err)
        }
    }
    

    async putErrorlogs(message:string){

        const params = {
            logGroupName: this.logGroupName,
            logStreamName: this.errorStreamName,
            logEvents: [
                {
                    message,
                    timestamp: new Date().getTime(),
                }
            ]
        }

        const logObject = await this.CloudWatch.getCloudWatchLogs()
        const writeCloudWatch = await logObject.putLogEvents(params).promise();

        console.log("Error Log Result :-->")
        console.log(writeCloudWatch)

    }

    async putDebuglogs(message:string){

        const params = {
            logGroupName: this.logGroupName,
            logStreamName: this.debugStream,
            logEvents: [
                {
                    message,
                    timestamp: new Date().getTime(),
                }
            ]
        }

        const logObject = await this.CloudWatch.getCloudWatchLogs()
        const writeCloudWatch = await logObject.putLogEvents(params).promise();

        console.log("Debug Log Result :-->")
        console.log(writeCloudWatch)

    }
}

const cloudWatchLogs = new awsConfigs(config.AWS_CRED.REGION,config.AWS_CRED.ACCESS_KEY_ID,config.AWS_CRED.SECRET_ACCESS_KEY).getCloudwatchObject()

// export default new CloudwatchService(cloudWatchLogs);

export default new PutLogs(new CloudwatchService(cloudWatchLogs));