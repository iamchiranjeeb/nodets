import AwsConfigs from './aws.config';
import { SQS } from "@aws-sdk/client-sqs";
import config from '../config/default';


class SQSObject {

    protected sqsInstance:SQS|null = null;
    protected sqsUrl:string|null = null;

    constructor(sqsObject: SQS,sqsUrl:string) {
        this.sqsInstance = sqsObject;
        this.sqsUrl = sqsUrl;
    }

    async sendMsg(message:any){
        
        if(this.sqsInstance && this.sqsUrl){
            await this.sqsInstance.sendMessage({
                QueueUrl: this.sqsUrl,
                MessageBody: message,
            })
        }
    }

}

const sqsObj = new AwsConfigs(config.AWS_CRED.REGION,config.AWS_CRED.ACCESS_KEY_ID,config.AWS_CRED.SECRET_ACCESS_KEY).getSqsObject()
export default new SQSObject(sqsObj,config.AWS_CRED.SQS.QUEUE_URL)