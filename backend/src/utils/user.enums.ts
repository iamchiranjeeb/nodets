export enum Expires {
    Yes= "enable",
    No= "disable"
}

export enum UserObjects {
    Blogs = "blogs",
    Todos = "todos",
    Followings = "followings",
    Followers = "followers"
}
