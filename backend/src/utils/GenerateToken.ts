import config from '../config/default';
import jsonwebtoken from 'jsonwebtoken';
import {DataStoredInToken,UserDocument} from '../interfaces/user.interface';


export async function createToken(user:UserDocument){
    const JWT_SECRET = config.JWT_SECRET
    const dataStoredInToken: DataStoredInToken = {_id: user._id}
    const token = jsonwebtoken.sign(dataStoredInToken, JWT_SECRET)
    user.tokens = user.tokens.concat({token})
    await user.save()
    return token
}