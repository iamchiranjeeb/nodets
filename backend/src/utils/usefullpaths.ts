import path from 'path';

class Paths {

    fileNameForUsers(file:Express.Multer.File){
        return `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`
    }

    getUsersBaseS3Path(userID:any){
        return `users/${userID}/`
    }

    getUsersBlogUploadPath(file:Express.Multer.File,userID:any): string {
        return `${this.getUsersBaseS3Path(userID)}/Blogs/${this.fileNameForUsers(file)}`.replaceAll("//", "/")
    }

}

export default new Paths();