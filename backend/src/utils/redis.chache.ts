import * as redis from 'redis';
import {RedisClientType} from 'redis';
import client from '../db/redis-connect';
import {Expires,UserObjects} from './user.enums';


class RedisOperations {

    async insertToRedis(key:string,value:string,Exp:Expires,expTime:number=5){

        try{
        
            // client.connect();
            const response = await client.set(key,value);
            console.log(`EXp:${expTime}`)

            if(Exp === Expires.Yes){
                await client.expire(key,expTime)
            }

            // client.quit();

        }catch(err){
            console.error("Reddis Error....)")
            console.error(err)
        }
    }

    async getFromReddis(key:string){
        try{

            // client.connect();
            const response = await client.get(key);
            // client.quit();
            return response

        }catch(err){
            console.error("Reddis Error..")
            console.error(err)
        }
    }

    async deleteFromReddis(key:string){
        client.connect();
        await client.del(key);
        client.quit();
    }

    async deleteCacheUserObjects(id:string){
        client.connect();
        const vals = Object.values(UserObjects);

        for(var i=0; i<vals.length; i++){


            await client.exists(`${id}-${vals[i]}`).then((value) => {

                if(value==1){
                     client.del(`${id}-${vals[i]}`);
                } else{ console.log(`${`${id}-${vals[i]}`} is not in reddis key.`) }

            }).catch(err => console.log(err))
        
        }

        client.quit();

    }

}


export default new RedisOperations();