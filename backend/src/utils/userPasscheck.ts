export function comparePasswords(pass1:string, pass2:string) {
    if (pass1 === pass2) {
        return true;
    }
    return false;
}

export function passwordValidation(pass:string){
    let lowerCase = /[a-z]/g;
    let upperCase = /[A-Z]/g;
    let specialCharacter = /[!@#$%?/\|:;~`^&*]/;
    let number = /[0-9]/g;

    let checkLengthPassword = pass.length < 8
    let checkSmallLetter = pass.match(lowerCase)
    let checkBigLetter = pass.match(upperCase)
    let checkNumberInPassword = pass.match(number)
    let checkSpecialCharacter = pass.match(specialCharacter)

    if(checkLengthPassword){
        console.log("length password is very small")
        return false;
    }else if(!checkBigLetter){
        return false;
    }else if(!checkSmallLetter){
        return false;
    }else if(!checkNumberInPassword){
        return false;
    }else if(!checkSpecialCharacter){
        return false;
    }else{
        return true;
    }
}
