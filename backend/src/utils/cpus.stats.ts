import os from 'os';


class CPUStats {

    calculateCpuUsageTimeout(){
        const { cpuUsage } = process;
    
        // Record the initial CPU usage
        const startUsage = cpuUsage();
    
        // Your code or task to measure CPU usage for
    
        // Record the CPU usage after your code has executed
        const endUsage = cpuUsage(startUsage);
    
        // Calculate the CPU time in microseconds
        const usedMicroSeconds = endUsage.user + endUsage.system;
    
        // Convert CPU time to milliseconds
        const usedMilliseconds = usedMicroSeconds / 1000;
    
        return usedMilliseconds
    }
    
    calculateCpuUsage() {
        const cpus = os.cpus();
        const totalCpuTime = cpus.reduce((acc, core) => {
          return acc + core.times.user + core.times.sys;
        }, 0);
      
        const processCpuTime = this.calculateCpuUsageTimeout()// Measure the CPU time used by your specific process/application here
      
        const cpuUsagePercentage = (processCpuTime / totalCpuTime) * 100;
        return cpuUsagePercentage;
    };
    
    memoryUsageInfo(){
    
        const totalMemory = os.totalmem();
    
        // Get the free memory (in bytes)
        const freeMemory = os.freemem();
    
        // Calculate memory usage as a percentage
        const memoryUsagePercentage = ((totalMemory - freeMemory) / totalMemory) * 100;
    
        const memoryData = {
            'Total Memory:':`${totalMemory} bytes`,
            'Free Memory:':`${freeMemory} bytes`,
            'Memory Usage:':`${memoryUsagePercentage.toFixed(2)}%`
        }
    
    
        return memoryData;
    
    }

}

export default CPUStats;