import { S3Client,DeleteObjectCommand } from '@aws-sdk/client-s3';
import { SQS } from "@aws-sdk/client-sqs";
import aws from 'aws-sdk';
require('aws-sdk/lib/maintenance_mode_message').suppress = true;

class AWSConfig {

    private region: string;
    private accessKeyId: string;
    private secretAccessKey: string;
    

    constructor(s3Region: string,accessKeyId:string,secretAccessKey:string) {
        this.region=s3Region;
        this.accessKeyId=accessKeyId;
        this.secretAccessKey=secretAccessKey;
    }

    getS3Configuration():S3Client{
        return new S3Client({
            region: this.region,
            credentials:{
                accessKeyId:this.accessKeyId,
                secretAccessKey:this.secretAccessKey
            }
        })
    }

    getSqsObject():SQS{
        const sqsClient = new SQS({
            region: this.region,
            credentials:{
                accessKeyId:this.accessKeyId,
                secretAccessKey:this.secretAccessKey
            }
        });
        return sqsClient
    }

    getCloudwatchObject():aws.CloudWatchLogs{

        const cloudwatchLogs = new aws.CloudWatchLogs({
            accessKeyId: this.accessKeyId,  
            secretAccessKey: this.secretAccessKey, 
            region: this.region,
        })

        return cloudwatchLogs
    }

    getAWSS3Object(){

        const s3 = new aws.S3({
            accessKeyId: this.accessKeyId,  
            secretAccessKey: this.secretAccessKey, 
            region: this.region,
          });
          console.log("coming to here")
          return s3;
    }

    async deleteS3Object(bucket:string, dir:string){

        const listParams = {
            Bucket: bucket,
            Prefix: dir
        };

        const listedObjects = await this.getAWSS3Object().listObjectsV2(listParams).promise();

        if(listedObjects.Contents !== undefined){
            if (listedObjects.Contents.length === 0) return;

            interface deleteParams {
                Bucket: string;
                Delete:{
                    Objects: any[];
                }
            }

            const deleteParams:deleteParams = {
                Bucket: bucket,
                Delete: { Objects: [] }
            };

            listedObjects.Contents.forEach(({ Key }) => {
                if(Key !== undefined){
                    deleteParams.Delete.Objects.push({ Key });
                }
            });

            await this.getAWSS3Object().deleteObjects(deleteParams).promise();
    
            if (listedObjects.IsTruncated) await this.deleteS3Object(bucket, dir);


        }


    }


}

export default AWSConfig;