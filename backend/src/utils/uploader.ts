import multer, { FileFilterCallback } from 'multer';
import RequestWithUser from '../interfaces/requestWithUser.interface'
import multerS3 from 'multer-s3';
import awsConfig from './aws.config';
import { AWSUploaderConfig } from '../interfaces/aws.interface';
import Paths from './usefullpaths';


class Uploader {

    s3BlogUploader(awsConfigs:AWSUploaderConfig){
        // console.log(awsConfigs);
        const uploadBlogsS3 = multer({
            storage:multerS3({
              s3:new awsConfig(awsConfigs.REGION,awsConfigs.ACCESS_KEY_ID,awsConfigs.SECRET_ACCESS_KEY).getS3Configuration(),
              bucket:awsConfigs.BUCKET_NAME,
              acl:"public-read",
              metadata:function (req,file,cb){
                cb(null,{fieldname:"BlogsDoc"})
              },
              key: function (req,file,cb){
                cb(null,Paths.getUsersBlogUploadPath(file,(req as RequestWithUser).user.id))
              },
              contentType:function(req,file,cb){
                cb(null,file.mimetype)
              }
            })
        })
        return uploadBlogsS3.array("file",10)
    }

}

export default new Uploader();