# Clone Repo

# Do yarn add

# Import Thunder-Client Collection in VS Code Thunder Client

# yarn dev to run

#### Create default.ts inside src/config/ && enrich with the following data

export default{
    port: port,
    host: host,
    mongoUri: mongoDBURL,
    reddisUri: reddisURL,
    JWT_SECRET: JwtSecret,
    AWS_CRED:{
        REGION:region,
        BUCKET_NAME: bucket,
        SECRET_ACCESS_KEY:secret-access-key,
        ACCESS_KEY_ID:access_key_id,
        CLOUD_WATCH:{
            LOG_GROUP:cloud_watch_log_group,
            ERROR_STREAM:log_stream_for_error_logs,
            DEBUG_STREAM:log_stream_for_debug_logs
        },
    }
};