#!/bin/bash

RED="\033[1;31m"
GREEN="\033[1;32m"
BLUE='\033[0;34m'
NOCOLOR="\033[0m"

DIR="node_modules"

nv=$(node -v)

function writeconfig() {

    file=$1

    defaultData="export default{
            port: 3001,
            host: '127.0.0.1',
            dbUri: 'mongourl',
            JWT_SECRET: 'secretkey'
    };"

    echo -e "Would You like to write default data : \c"
    read ans
    if [ "${ans}" = "yes" ] || [ "${ans}" = "y" ] || [ "${ans}" = "Y" ] || [ "${ans}" = "Yes" ]
    then
        echo $defaultData >> $1
        echo "Exiting..."
        echo "With Default Data Application Will not work"
        exit
    else
        echo -e "Enter PORT $ \c"
        read port
        echo -e "Enter HOST $ \c"
        read host
        echo -e "Copy Paste Mongo-URL $ \c"
        read url
        echo -e "Enter JWT_SECRET $ \c"
        read jwtSecret
        ownData="export default{
            port: "${port}",
            host: '"${host}"',
            dbUri: '"${url}"',
            JWT_SECRET: '"${jwtSecret}"'
        };"
        echo $ownData >> $1
    fi
}

function nodesetup() {

    if [ ! -d "${DIR}" ]
    then
        echo "${DIR} does not Exists"
        echo "Installing node_modules..."
        yarn install
    fi

    if [ -d "${DIR}" ]
    then
        echo "${DIR} found."
        echo "Starting node application..."
        yarn dev
    fi
}

function startnode() {

    echo -e "Would You Like to Start Node Application: \c"
    read ans
    if [ "${ans}" = "yes" ] || [ "${ans}" = "y" ] || [ "${ans}" = "Y" ] || [ "${ans}" = "Yes" ]
    then
        nodesetup
    else
        exit
    fi
}

function checkconfigdir() {

    CONFIG_DIR="config"
    CONFIG_FILE="default.ts"

    if [ ! -d "${CONFIG_DIR}" ]
    then
        mkdir "${CONFIG_DIR}"
        cd "${CONFIG_DIR}"
        touch "${CONFIG_FILE}"
        writeconfig $CONFIG_FILE
        cd ..
        startnode
    else
        cd "${CONFIG_DIR}"
        if [ -f "${CONFIG_FILE}" ]
        then
            if [ -s "${CONFIG_FILE}" ]
            then
                echo "default.ts is not empty"
                echo -e "Have You Verified the data $ \c"
                read ans
                if [ "${ans}" = "yes" ] || [ "${ans}" = "y" ] || [ "${ans}" = "Y" ] || [ "${ans}" = "Yes" ]
                then
                    cd ..
                    startnode
                else
                    echo "Exiting...."
                    echo "Verify Then Start Application.."
                    exit
                fi
            else
                echo "Empty File Found!"
                writeconfig $CONFIG_FILE
                cd ..
                pwd
                startnode
            fi
        else
            touch $CONFIG_FILE
            writeconfig $CONFIG_FILE
            cd ..
            pwd
            startnode
        fi
        cd ..
    fi
}


function askconfigoptions() {
    echo -e "${GREEN}0->Run Node Application${NOCOLOR}"
    echo -e "${GREEN}1->Start Config Setup${NOCOLOR}"
    echo "Would you like to go for Config Setup Then Run Application / Directly Run Node Application ?"
    echo -e "ENTER OPTION : \c"
    read opt
    if [ "${opt}" -eq 0 ]
    then
        startnode
    elif [ "${opt}" -eq 1 ]
    then
        checkconfigdir
    else
        echo -e "${RED}ERROR: Unknown Option${NOCOLOR}"
    fi

}

if [[ $nv =~ "v18" ]] || [[ $nv =~ "v17" ]] || [[ $nv =~ "v16" ]] || [[ $nv =~ "v15" ]]
then
    echo -e "${GREEN}Node Found. ${nv}${NOCOLOR}"
    askconfigoptions
elif [[ $nv =~ "v" ]]
then
    echo -e "${BLUE}Found Older Node Version.${nv}${NOCOLOR}"
    echo -e "Would You Like to continue : \c"
    read response
    if [ "${response}" = "yes" ] || [ "${response}" = "y" ] || [ "${response}" = "Y" ] || [ "${response}" = "Yes" ]
    then
        askconfigoptions
    else
        echo "Exiting..."
        exit
    fi
else
    echo -e "${GREEN}Could not Find Node. ${nv}${NOCOLOR}"
fi
