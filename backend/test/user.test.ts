import request from 'supertest';
import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';
import app from '../src/app';


describe('users',() => {

    beforeAll(async () => {
        const mongoServer = await MongoMemoryServer.create();

        // await mongoose.connect(mongoServer.getUri());
    })


    afterAll(async () => {
        await mongoose.disconnect();
        await mongoose.connection.close();
    })


    describe('get all users',() => {
        it("It should return all users",async () => {
           const {body,statusCode} =  await request(app).get('/api/get/users')
           
           expect(statusCode).toBe(200);

           expect(body)
        })
    })
})