import request from 'supertest';
import { MongoMemoryServer } from 'mongodb-memory-server';
import {createToken} from '../src/utils/GenerateToken';
import mongoose from 'mongoose';
import config from 'config';
import jsonwebtoken from 'jsonwebtoken';
import {DataStoredInToken} from '../src/interfaces/user.interface'
import app from '../src/app';

const userId = new mongoose.Types.ObjectId().toString()
const JWT_SECRET = config.get<string>('JWT_SECRET')

export const todoPayload = {
    todoName: "chiru-looser",
    isDone: false,
    owner:userId
}

export const userPayload = {
    _id:userId,
    email:"chiru100@outlook.com",
    name:"looser",
    password:"chiru",
    confirmPassword:"chiru"
}


describe('todos', () => {

    beforeAll(async () => {
        const mongoServer = await MongoMemoryServer.create();

        // await mongoose.connect(mongoServer.getUri());
    })


    afterAll(async () => {
        await mongoose.disconnect();
        await mongoose.connection.close();
    })

    describe('given the user is not logged in', () => {
        it('should return 401 status', async() => {
            const {status} = await request(app).post('/api/insert/todo')

            expect(status).toBe(401)
        })
    })

    // describe('given the user is logged in', () => {
    //     it('should return 200 status', async() => {
    //         const dataStoredInToken: DataStoredInToken = {_id: userId}
    //         const token = jsonwebtoken.sign(dataStoredInToken, JWT_SECRET)

    //         const {status,body} = await request(app).post('/api/insert/todo')

    //         .set(`Authorization`,`Bearer ${token}`)

    //         .send(todoPayload)

    //         expect(status).toBe(200)

    //         expect(body)
    //     })
    // })


})