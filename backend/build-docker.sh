#!/bin/bash

RED="\033[1;31m"
GREEN="\033[1;32m"
BLUE="\033[0;34m"
NOCOLOR="\033[0m"


echo -e "${RED}Mongourl && Reddisurl are not getting Replaced Currently.${NOCOLOR}"

read -e -p "Enter Port No: " originalPort
composeFilePort="'$originalPort:$originalPort'"
configFilePort=$originalPort,


read -e -p "Enter Host: " host
finalHost="'$host',"
read -e -p "Enter MongoDB URL: " mongourl
read -e -p "Enter Reddis URL: " reddis
read -e -p "Enter Container Name: " containername


dockerFile="Dockerfile"
composeFile="docker-compose.yml"
defaultConfigFile="src/config/default.ts"


portInComposeFile=`cat $composeFile | grep -`
containerNameinComposeFile=`cat $composeFile | grep container_name:`
portInDockerFile=`cat $dockerFile | grep EXPOSE`
portInDefaultConfigFile=`cat $defaultConfigFile | grep port:`
hostInDefaultConfigFile=`cat $defaultConfigFile | grep host:`
mongoUriInDefaultConfig=`cat $defaultConfigFile | grep mongoUri:`
reddisUriInDefaultConfig=`cat $defaultConfigFile | grep reddisUri:`


currentPortComposeFile=`echo "$portInComposeFile" | awk -F"- " '{print $2}'`
currentContainerNameComposeFile=`echo "$containerNameinComposeFile" | awk -F"container_name: " '{print $2}'`
currentPortinDockerFile=`echo "$portInDockerFile" | awk -F"EXPOSE " '{print $2}'`
cuurentDefaultFilePort=`echo "$portInDefaultConfigFile" | awk -F"port: " '{print $2}'`
cuurentDefaultFileHost=`echo "$hostInDefaultConfigFile" | awk -F"host: " '{print $2}'`
currentDefaultFileMongoUri=`echo "$mongoUriInDefaultConfig" | awk -F"mongoUri: " '{print $2}'`
currentDefaultFileReddisUri=`echo "$reddisUriInDefaultConfig" | awk -F"reddisUri: " '{print $2}'`


echo $currentDefaultFileMongoUri

sed -i "s/$currentPortComposeFile/$composeFilePort/" $composeFile
sed -i "s/$currentContainerNameComposeFile/$containername/" $composeFile
sed -i "s/$currentPortinDockerFile/$originalPort/" $dockerFile
sed -i "s/$cuurentDefaultFilePort/$configFilePort/" $defaultConfigFile
sed -i "s/$cuurentDefaultFileHost/$finalHost/" $defaultConfigFile
# sed -i "s/$currentDefaultFileMongoUri/$mongourl/" $defaultConfigFile
# sed -i "s/$currentDefaultFileReddisUri/$reddis/" $defaultConfigFile


##Checkdist dir is present
if [ -d dist ]
then
    echo -e "Deleting Existing dist Directory."
    echo -e "Building The Project."
    yarn run build
else
    echo -e "Building The Project."
    yarn run build
fi

if [ -d dist ]
then
    echo -e "Building Finished..."
else
    echo -e "Error While Building The Project..."
    exit 1
fi

yarn add .

sudo docker-compose up -d