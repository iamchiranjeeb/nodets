

const TestButton = (props) =>{

    const  testClick= () =>{
        alert("Not your cup of tea...")
        console.log("testing here")
    }

    return (
        // <input type="text" />
        <button onClick={testClick}>{props.buttonName}</button>
    )
}

export default TestButton