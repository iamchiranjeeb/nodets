import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import React from 'react';
import './App.css';
import AppRouters  from './Routers/Routers';
import AuthState from './contexts/Auth/AuthState'

function App() {

  return (

    <>

      <AuthState>

        <div className="container">

          <AppRouters/>
          
        </div>

      </AuthState>

    </>

  )
}

export default App;
