import { configureStore } from '@reduxjs/toolkit'
import OtherUserReducer from './Slices/OtherUser'
import DashboardReducer from './Slices/Dashboard'

const Store = configureStore({

    reducer:{
        OtherUserReducer:OtherUserReducer,
        DashboardReducer
    }

})

export default Store