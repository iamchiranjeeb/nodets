import { createSlice } from '@reduxjs/toolkit'

const DashboardSlice= createSlice({

    name:'Dashboard_Slice',

    initialState:{
        profile_data:{},
        all_followings:[],
        all_posts:[],
        all_followers:[]
    },

    reducers:{

        set_profile_data: (state,action)=>{
            state.profile_data = action.payload
        },

        set_all_followings: (state,action)=>{
            state.all_followings = action.payload
        },

        set_all_followers: (state,action)=>{
            state.all_followers = action.payload
        },

        set_all_posts: (state,action)=>{
            state.all_posts = action.payload
        }

    }

})

export const {set_initial_profile_data,set_all_followings,set_see_followings,set_all_posts,set_all_followers} = DashboardSlice.actions;
export default DashboardSlice.reducer;