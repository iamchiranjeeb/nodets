import { createSlice } from '@reduxjs/toolkit'


const OtherUserSlice = createSlice({

    name: 'OtherUser_Slice',
    initialState :{
        total_followers: 0
    },
    reducers:{

        increment_followers_by_one:(state,action)=>{
            state.total_followers += 1
        },

        set_initial_followers_value:(state,action)=>{
            state.total_followers = action.payload
        },

        decrement_followers_by_one:(state,action)=>{
            state.total_followers -= 1
        }

    }

})

export const {increment_followers_by_one,set_initial_followers_value,decrement_followers_by_one} = OtherUserSlice.actions;
export default OtherUserSlice.reducer;