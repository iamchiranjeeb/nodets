import React, {useState,useEffect} from "react";


function Counter () {

    const [count,setCount] = useState(10);
    const [users,setUsers] = useState([]);
    const [isCorrect,setifCorrect] = useState(false);
    let content = null;

    //useState Hook return array with two values
    //first part count is the value of currentState
    //second part setCount is used to update the current state

    function decrementCount(){
        setCount(prevCount => prevCount - 1)
    }

    function incrementCount(){
        setCount(prevCount => prevCount + 1)
    }


    const getUserDatas = async()=>{
        const res = await fetch('http://0.0.0.0:3001/api/get/users')
        const jsonDatas = await res.json();
        console.log("Json Datas ...")
        console.log(jsonDatas)
        setUsers(jsonDatas.message)
        setifCorrect(jsonDatas.success)
    }

    useEffect(() => {
        console.log('Looser !!!')
        getUserDatas();
    },[])

    if(isCorrect) {
        console.log("coming here ...")
        content = (
            <div>

                {
                    users.length > 0 ? (
                        users.map((d,i) => (
                            <h2>{d.name} {d.email}</h2>
                        ))
                    ):(
                        console.log("no users")
                    )
                }
                
            
        
                
            </div>
        )
        console.log("jjj")

        users.map((d,i)=>{
            console.log(d.email)
        })
       
    }else{

        content = (
            <div>
                <button onClick={incrementCount}>+</button>
                <span>{count}</span>
                <button onClick={decrementCount}>-</button>
            </div>
        )

    }

    return (
        <div>
            {content}
        </div>
    )

}


export default Counter