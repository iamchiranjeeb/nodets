import React,{useEffect,useState} from 'react';
import AuthContext from './AuthContext';


function AuthState(props){

    const [isAuthenticated,setIsAuthenticated] = useState(false);

    useEffect(() => {
        const token = localStorage.getItem('my_token')
        console.log("Token is Auth Context " + token)
  
        if(token) {
            setIsAuthenticated(true);
        }else{
            localStorage.removeItem('my_token');
            setIsAuthenticated(false);
        }
  
    },[])

    return (

        <AuthContext.Provider value={{isAuthenticated,setIsAuthenticated}}>
            {props.children}
        </AuthContext.Provider>

    )

}

export default AuthState