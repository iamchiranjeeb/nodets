import React, { createContext, useState, useEffect } from "react";

const defaultContext = {
    isAuthenticated:false
}

const AuthContext = createContext(defaultContext)

export default AuthContext;
