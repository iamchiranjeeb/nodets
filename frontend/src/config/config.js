

const EnvConfig = {
    BASE_URL:"http://0.0.0.0:3001/api",
    BACKEND_IP:"127.0.0.1",
    BACKEND_PORT:"3001",
}

export default EnvConfig