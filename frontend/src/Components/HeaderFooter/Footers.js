import React from 'react';
import '../css/components.css'

function Footer(){

    return (
        <footer><h3>{new Date().toLocaleDateString()} All Copy rights reserved.</h3></footer>
    )

}

export default Footer