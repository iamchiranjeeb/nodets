import React,{useState,useRef} from "react";
import axios from "axios";
import EnvConfig from '../../config/config'
import ToggleSwitch from './Buttons/ToggleSwitch'


function Signup({history}){

    //States for all DOM
    const [email,setEmail] = useState("");
    const [username,setUserName] = useState("");
    const [password,setPassword] = useState("");
    const [confirmPassword,setConfirmPassword] = useState("");
    const [isPrivateAccount,setIfPrivateAccount] = useState(false);

    //Refs for DOM
    const greetMsg = useRef("")
    const errorMessage = useRef("")
    const passwordRef = useRef("")
    const confirmPasswordRef = useRef("")



    const emailChangeHandler = (event) => {
        setEmail(event.target.value);
    }

    const userChangeHandler = (event) => {
        setUserName(event.target.value);
    }

    const passwordChangeHandler = (event) => {
        setPassword(event.target.value);
    }

    const confirmPasswordHandler = (event) => {
        setConfirmPassword(event.target.value);
    }

    const setToggleHandler = (event) => {
        setIfPrivateAccount(!isPrivateAccount);
    }



    const submitHandler = (event) => {
        event.preventDefault();
        // document.getElementById("greet-msg").textContent = `Hello ${username}`
        greetMsg.current.textContent = `Hello ${username} Welcome to you.`;

        if(password!=confirmPassword){
            // document.getElementById("error-message").textContent = "Passwords did not match."
            errorMessage.current.textContent = `Passwords did not match....`;
            alert('Password & Confirm Password should match.');
            setConfirmPassword("")
            setPassword("")
            confirmPasswordRef.current.style.borderColor = 'red';
            passwordRef.current.style.borderColor = 'red';
            return
        }

        const formData = {
            email,
            name:username,
            password,
            confirmPassword,
            isPrivateAccount
        }

        axios.post(`${EnvConfig.BASE_URL}/user`,formData,{

            //this validate is responsible for redirecting status code 400 into then instead catch
            validateStatus: function (status) {
                return status == 200 || status == 201 || status == 400
            }

        }).then((response) => {

            console.log("resp is")
            console.log(response)

            if(response.status==200 || response.status==201){
                setConfirmPassword("")
                setPassword("")
                setEmail("")
                window.location.href = "/login"
                // document.getElementById("success-message").textContent = `Account Created for ${email}`
            }else if(response.status==400){
                errorMessage.current.textContent = response.data.message
                alert(response.data.message)
            }else{
                errorMessage.current.textContent = response.data.message
                alert(response.data.message)
            }

        })
        .catch((err) => {
            console.error("Error is ")
            console.error(err)
            errorMessage.current.textContent = err.message
            alert(err.message)
        })


    }



    return(

        <div>

            <h1>Signup Page</h1>
            <br/><br/>

            <form onSubmit={submitHandler}>

                <div>
                    <label htmlFor="email">Email:</label>
                    <input type="email" value={email} id="email" name="email" onChange={emailChangeHandler}/>
                </div>

                <br/>

                <div>
                    <label htmlFor="username">Username:</label>
                    <input type="text" value={username} id="username" name="username" onChange={userChangeHandler}/>
                </div>

                <br/>

                <div>
                    <label htmlFor="password">Password:</label>
                    <input type="password" ref={passwordRef} value={password} id="password" name="password" onChange={passwordChangeHandler}/>
                </div>

                <br/>

                <div>
                    <label htmlFor="c-password">Password:</label>
                    <input type="password" ref={confirmPasswordRef} value={confirmPassword} id="c-password" name="c-password" onChange={confirmPasswordHandler}/>
                </div>

                <br/>

                <label>Private Account ?</label> 
                <ToggleSwitch initValue={isPrivateAccount} onToggle={setToggleHandler}/>

                <br/>

                <div>
                    <button type="submit">Submit</button>
                </div>

            </form>

            <p id="show-email">Email : {email}</p>
            <p id="show-user">Username : {username}</p>
            <p id="greet-msg" ref={greetMsg}></p>
            <p id="error-message" ref={errorMessage}></p>
            <p id="success-message"></p>

        </div>

    )


}

export default Signup;