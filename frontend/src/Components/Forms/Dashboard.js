import React,{useEffect,useState} from 'react';
import { Route, useNavigate,NavLink,Link } from "react-router-dom";
import {AfterLogin} from './Users/FindUsers';
import axios from "axios";
import '../../css/Dashboard.css';
import {ZoomableImage,ZoomableVideo} from './FormUtils/ZoomedImages'
import ToggleButton from './Buttons/ToggleSwitch';
import BlogForm from './Posts/Posts';
import {ViewPosts,ViewStatus} from './FormUtils/DasboardUtils';
import {set_all_followings,set_all_posts,set_all_followers} from '../../RTK/Slices/Dashboard';
import { useSelector, useDispatch } from 'react-redux';
import {getPosts,getDashboardDatas,followingViewersData,followersData} from './apis/dashboard';

function Dashboard() {

    const [authenticatedUser,isAuthenticatedUser] = useState(false);
    const [myProfile,setMyProfile] = useState({});
    const [findUser,setFindUser] = useState(false);
    const [showBlogForm,setShowBlogForm] = useState(false);
    const [seeFollowings,setSeeFollowings] = useState(false);
    const [seeFollowers,setSeeFollowers] = useState(false);

    const followings = useSelector(state=>state.DashboardReducer.all_followings)
    const followers = useSelector(state=>state.DashboardReducer.all_followers)
    const posts = useSelector(state=>state.DashboardReducer.all_posts)

    const linkName=seeFollowings?'___Hide Followings___':'___View Followings___'

    const linkNameFollowers=seeFollowers?'___Hide Followers___':'___View Followers___'

    const dispatch = useDispatch()

    const setFindUserHandler = () => {
        setFindUser(!findUser);
        if(showBlogForm){
            setShowBlogForm(!showBlogForm)
        }
    }

    const setShowBlogFormHandler = () => {
        setShowBlogForm(!showBlogForm);
        if(findUser){
            setFindUser(!findUser);
        }
    }

    const navigate = useNavigate();

    useEffect(() => {

        const token = localStorage.getItem('my_token')
        let dashboardIntervalId;
        let postIntervalId;
        let followingsIntervalId;
        let followersIntervalId;

        if(token){
            isAuthenticatedUser(true);

            getDashboardPage(token)

            dashboardIntervalId = setInterval(() => {
                getDashboardPage(token)
            },3000)

        }else{
            navigate('/login')
        }

        allPosts()

        postIntervalId = setInterval(() => {
            allPosts()
        },3000)

        if(token && seeFollowers) {

            followersIntervalId = setInterval(() => {
                allFollowers(token)
            },4000)

        }

        if(token && seeFollowings){

            followingsIntervalId = setInterval(() => {
                allFollowings(token)
            },4000)

        }

        return ()=>{

            if(dashboardIntervalId){
                console.log('Clearing Dashboard Interval....')
                clearInterval(dashboardIntervalId)
            }

            if(postIntervalId){
                console.log('Clearing Post Interval....')
                clearInterval(postIntervalId)
            }

            if(followersIntervalId){
                clearInterval(followersIntervalId)
            }

            if(followingsIntervalId){
                clearInterval(followingsIntervalId)
            }

        }



    },[])

    const getDashboardPage = async(token) =>{

        const dashboardResp = await getDashboardDatas(token)

        if(dashboardResp.status == 401){

            isAuthenticatedUser(false)
            localStorage.removeItem('my_token');
            navigate('/login')

        }else if(dashboardResp.status == 200 || dashboardResp.status == 201){

            console.log(dashboardResp.data)
            setMyProfile(dashboardResp.data.message)

        }else{

            alert('Error: ' + dashboardResp.status)

        }
       
    }

    const allPosts = async() => {
        var posts = await getPosts()
        dispatch(set_all_posts(posts))
    }

    const allFollowings = async(token) => {
        const resp = await followingViewersData(token)
        if(resp.status == 200) {
            dispatch(set_all_followings(resp.data.followings))
        }
    }

    const allFollowers = async(token) => {
        const resp = await followersData(token)
        if(resp.status == 200) {
            dispatch(set_all_followers(resp.data.followings))
        }
    }


    const followingViewers = async () =>{

        const token = localStorage.getItem('my_token')

        if(token){

            const resp = await followingViewersData(token)

            if(resp.status === 200){
                dispatch(set_all_followings(resp.data.followings))
                setSeeFollowings(!seeFollowings)
            }
            
        }

    }

    const followersViewers = async()=>{

        const token = localStorage.getItem('my_token')

        if(token){

            const resp = await followersData(token)

            if(resp.status === 200){
                dispatch(set_all_followers(resp.data.followers))
                setSeeFollowers(!seeFollowers)
            }
            
        }

    }

    return (

        <div>

            {

                authenticatedUser ? (

                    <div className='dashboardContainer'>

                        <div className="dashboard1">

                            {<ViewStatus isLoggedIn={myProfile.isLoggedIn} textRequire={true}/>}

                            <h3>{myProfile.name}</h3>
                            <h3>{myProfile.email}</h3>

                            {
                                myProfile.totalfollwers > 0 ? (
                                    <div>
                                        <h3>{myProfile.totalfollwers} Followers</h3>
                                    </div>
                                ):(
                                    <h3>No One Follows You</h3>
                                )
                            }

                            {
                                myProfile.totalfollowings > 0 ? (
                                    <div>
                                        <h3>{myProfile.totalfollowings} Followings</h3>
                                    </div>
                                ):(
                                    <h3>You have not followed any one</h3>
                                )

                            }

                            <ToggleButton initValue={findUser} onToggle={setFindUserHandler}/>

                            <ToggleButton initValue={showBlogForm} onToggle={setShowBlogFormHandler}/> 

                            <Link className="read-more-link" onClick={followingViewers}><h3>{linkName}</h3></Link>

                            {
                                seeFollowings && followings.map((val,idx)=>(
                                    <div style={{ display: 'flex', alignItems: 'center' }}>
                                        <h3 style={{marginRight: '10px'}}><NavLink key={val._id} to={`/user/${val._id}`}>{val.name}</NavLink></h3>
                                        {<ViewStatus isLoggedIn={val.isLoggedIn} textRequire={false}/>}
                                        <br/>
                                    </div>
                                ))
                            }

                            <Link className="read-more-link" onClick={followersViewers}><h3>{linkNameFollowers}</h3></Link>

                            {
                                seeFollowers && followers.map((val,idx)=>(
                                    <div style={{ display: 'flex', alignItems: 'center' }}>
                                        <h3 style={{marginRight: '10px'}}><NavLink key={val._id} to={`/user/${val._id}`}>{val.name}</NavLink></h3>
                                        {<ViewStatus isLoggedIn={val.isLoggedIn} textRequire={false}/>}
                                        <br/>
                                    </div>
                                ))
                            }

                        </div>

                        {
                            <ViewPosts posts={posts} />
                        }

                        <div className='serachUser'>

                            {

                                findUser ? (
                                    <AfterLogin/>
                                ):(
                                    <br/>
                                )

                            }

                            {

                                showBlogForm ? (
                                    <BlogForm/>
                                ):(
                                    <br/>
                                )

                            }

                        </div>     
                        
                       

                    </div>


                ) : (
                    navigate('/login')
                )

            }

        </div>

    )


}

export default Dashboard;