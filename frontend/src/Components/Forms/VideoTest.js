import React, {useRef, useEffect, useState} from 'react'
import YouTube from 'react-youtube';


function VideoPlayer(){

    const videoRef = useRef(null)

    const videoId = 'XfX2Ap30pwU';

    useEffect(()=>{
        if(videoRef.current){
            videoRef.current.pause()
            videoRef.current.removeAttribute('src')
            videoRef.current.load()
        }
    })

    // Optional: Add event handlers
    const onReady = (event) => {
        // access to player in all event handlers via event.target
        event.target.pauseVideo();
    };

    const opts = {
        height: '240',
        width: '390',
        playerVars: {
          // https://developers.google.com/youtube/player_parameters
          autoplay: 0,
        },
    };

    return(
        <div>
            <h2>Video Player</h2>

            <video ref={videoRef} width='390' height='240' controls>
                <source src={`http://localhost:3001/api/play/video/cdn`} type='video/mp4'></source>
                Your browser does not support the video tag.
            </video>

            <YouTube videoId={videoId} opts={opts} onReady={onReady} />;
        </div>
    )

}



export default VideoPlayer;