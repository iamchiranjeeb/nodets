import React,{useState,useRef} from "react";
import axios from "axios";
import EnvConfig from '../../config/config'



function Login() {

    const [email,setEmail] = useState("")
    const [password,setPassword] = useState("")

    //Refs for DOM
    const errorMessageRef = useRef("")
    const successMessageRef = useRef("")


    const setEmailHandler = (event) =>{
        setEmail(event.target.value)
    }

    const setPasswordHandler = (event) =>{
        setPassword(event.target.value)
    }

    const submissionHandler = async (event) => {
        event.preventDefault()
        const formData ={
            email,
            password
        }

        try{

            const response = await axios.post(`${EnvConfig.BASE_URL}/login/user`,formData,{

                validateStatus: function(status) {
                    return status === 200 || status === 400 || status === 401 ||status === 500 || status === 404
                }

            })

            console.log("Response is : ")
            console.log(response)

            if(response.status==200){
                successMessageRef.current.textContent = response.data.token
                localStorage.setItem('my_token', response.data.token);
                window.location.href = "/dashboard"
            }else{
                errorMessageRef.current.textContent = response.data.message
                alert(response.data.message)
                setPassword("")
                setEmail("")
            }


            
        }catch(e){
            console.log("Error While doing login ")
            console.log(e)
            errorMessageRef.current.textContent = e.message
            alert(e.message)
        }


    }


    return(

        <div>

            <h1>Login Page</h1>
            <br/><br/>

            <form onSubmit={submissionHandler}>

                <div className="login-form">

                    <div>
                        <label htmlFor="email">Email:</label>
                        <br/>
                        <input type="email" id="email" name="email" value={email} onChange={setEmailHandler}/>
                    </div>

                    <br/>

                    <div>
                        <label htmlFor="password">Password:</label>
                        <br/>
                        <input type="password" id="password" name="password" value={password} onChange={setPasswordHandler} />
                    </div>

                    <br/>

                    <div>
                        <button type="submit">Submit</button>
                    </div>

                </div>

            </form>

            <p ref={errorMessageRef}></p>
            <p ref={successMessageRef}></p>

        </div>

    )

}

export default Login