import React from "react";
import '../../../css/Home.css'
import {MemoryData,CPUData,SystemData} from './CPUS/CPUS';


function MainUtils({pcData,cpuData,memoryData}){

    return (

        <div className="main_home_page">

            <div className="system_box">

                <SystemData systemData={pcData}/>

                </div>

                <br/>

                <div className="cpu-memory-container">

                    <div className="cpu_box">

                        <CPUData cpuDatas={cpuData}/>

                    </div>

                    <div className="memory_box">

                        <MemoryData memory={memoryData}/>

                    </div>

                </div>

        </div>

    )

}

export default MainUtils