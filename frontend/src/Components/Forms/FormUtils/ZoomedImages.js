import React, { useState } from 'react';
import './utils.css'

const ZoomableImage = ({ imageUrl }) => {
  const [zoomed, setZoomed] = useState(false);

  const handleImageClick = () => {
    setZoomed(!zoomed);
  };

  return (
    <div className="zoomable-image-container" onClick={handleImageClick}>
        <img src={imageUrl} alt="Zoomable Image" className="zoomable-image" />
    </div>
  );
};

const ZoomableVideo = ({ videoUrl }) => {
    const [zoomed, setZoomed] = useState(false);
  
    const handleImageClick = () => {
      setZoomed(!zoomed);
    };
  
    return (
  
      <div className="zoomable-image-container">
            <video controls className='zoomable-video'>
              <source src={videoUrl} type='video/mp4'></source>
              Your browser does not support the video tag.
            </video>

      </div>
    );
  };

export {ZoomableVideo,ZoomableImage}
