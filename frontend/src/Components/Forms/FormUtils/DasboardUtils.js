import React,{useEffect,useState} from 'react';
import '../../../css/Dashboard.css'
import {ZoomableImage,ZoomableVideo} from './ZoomedImages'
import {deleteSinglePost} from '../apis/dashboard'


function ViewStatus({isLoggedIn,textRequire}){

    return (
        <div>
            {
                isLoggedIn ? (
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        {
                            textRequire && (
                                <h3 style={{ color: 'lightgreen',marginRight: '3px' }}>Online </h3>
                            )
                        }
                        <div style={{ width: '10px', height: '13px', background: 'green', borderRadius: '60%' }}></div>
                    </div>
                ):(

                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        {
                            textRequire && (
                                <h3 style={{ color: 'red',marginRight: '3px' }}>Offline </h3>
                            )
                        }
                        <div style={{ width: '10px', height: '13px', background: 'red', borderRadius: '60%' }}></div>
                    </div>

                )
            }
        </div>
    )

}

function ViewPosts({posts}){

    const deletePost = (id)=>{
        deleteSinglePost(id)
    }

    return (

        <>
            <div className='ownPosts'>

                <h2>Posts.....</h2>
                <br/>

                {

                    posts.length > 0 && posts.map((val,idx)=>(

                        <div>

                            <h3>{val.header}</h3>
                            {/* <button className="delete-button" onClick={deletePost(val._id)}>X</button> */}
                            {
                                val.blogposts.length > 0 && (
                                    <div style={{ display: 'flex' }}>
                                        {
                                            val.blogposts.map((singleBlog,blogIdx) => (

                                                ((singleBlog.contentType === 'image/jpeg' || singleBlog.contentType === 'image/png') && (
                                                    <ZoomableImage key={blogIdx} imageUrl={singleBlog.location} />
                                                  )) ||
                                                  (singleBlog.contentType === 'video/mp4' && (
                                                    <ZoomableVideo videoUrl={singleBlog.location}/>
                                                  ))            

                                            ))
                                        }
                                    </div>

                                )
                            }

                        </div>

                    ))

                }

            </div>
        </>

    )
}

export {ViewPosts,ViewStatus}