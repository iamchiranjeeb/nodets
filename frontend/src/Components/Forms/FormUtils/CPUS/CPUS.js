import React from "react";

function MemoryData({memory}) {

    return (
        <div>
            {
                memory.map((val,idx)=>(
                    <h2 key={idx}>{val}</h2>
                ))
            }
        </div>
    )

}


function CPUData({cpuDatas}) {

    return (
        <div>
            {
                cpuDatas.map((val,idx) => (
                    <h2 key={idx}>{val}</h2>
                ))
            }
        </div>
    )

}


function SystemData({systemData}) {

    return (
        <div>
            {
                systemData.map((val,idx) => (
                    // <h2 key={index} style={{ color: index % 2 === 0 ? 'blue' : 'red' }}>{val}</h2>
                    <h2 key={idx}>{val}</h2>
                    // <h2 key={index} style={{ color: index % 3 === 0 ? 'blue' : index % 3 === 1 ? 'red' : 'green' }}>{val}</h2>
                ))
            }
        </div>
    )

}


export {MemoryData,CPUData,SystemData}