import React,{useState,useEffect} from "react";
import { useNavigate,BrowserRouter as Router, Route, Routes } from "react-router-dom";
import EnvConfig from '../../../config/config'
import axios from "axios";
import TestElement from "./UserPage";
import { NavLink,Link } from 'react-router-dom';


function AfterLogin(){

    const [user,setUser] = useState("")
    const [userList,setUserList] = useState("")

    const findUserHandler = (event) => {
        setUser(event.target.value)
    }

    async function formSubmissionHandler(event) {

        const token = localStorage.getItem('my_token')

        try{

            if(token) {

                const formData = {
                    "user_string":user
                }

                if(!user){
                    setUserList("")
                    return
                }

                const response = await axios.post(`${EnvConfig.BASE_URL}/findUser`,formData,{

                    headers:{
                        Authorization: `Bearer ${token}`
                    },

                    validateStatus: function(status) {
                        return status === 200 || status === 400 || status === 401 ||status === 500 || status === 404
                    }
    
                })

                if(response.status==200){

                    console.log("Response-->")
                    console.log(response.data)
                    setUserList(response.data.message)
                    
                }else{
                    console.log("4000")
                    console.log(response.data)
                }


            }else{
                console.log("40111")
            }

        }catch(e){
            console.log("if heerer")
            console.log(e)
        }
    }

    return (

        <div>


            <h2>Find User</h2>
            <input type="text" value={user} id="find-user" name="find-user" onChange={findUserHandler} />
            <br/>

            <button onClick={formSubmissionHandler}>Find</button>

            <br/>

            {

                userList.length > 0 ? (

                    <div key={user._id}>


                        <nav>

                            <ul>

                                {
                                    userList.map((user) => (

                                        <div>

                                            <br/>
                                            

                                            <li>

                                                <h3><NavLink key={user._id} to={`/user/${user._id}`}>{user.name}</NavLink></h3>

                                            </li>

                                        </div>

                                    ))

                                    
                                }

                            </ul>

                        </nav>


                    </div>
                    
                ):(
                    <h2>No Users Found</h2>
                )

            }


        </div>

    )

}

export {AfterLogin}