import React,{useState,useEffect,useRef} from "react";
import { useParams,useNavigate } from 'react-router-dom';
import axios from "axios";
import EnvConfig from '../../../config/config'
import { useSelector, useDispatch } from 'react-redux'
import {ViewStatus} from '../FormUtils/DasboardUtils'
import {increment_followers_by_one,set_initial_followers_value,decrement_followers_by_one} from '../../../RTK/Slices/OtherUser'

function TestElement(){
    const { userId } = useParams();
    const navigate = useNavigate();
    const [userData,setUserData] = useState({})
    const [isLoading,setIsLoading] = useState(true)
    const [followUnfollowMsg,setFollowUnfollowMsg] = useState("")
    const followMsgRef = useRef("")
    const token = localStorage.getItem('my_token')

    const currFollowers = useSelector(state=>state.OtherUserReducer.total_followers)
    const dispatch = useDispatch()


    useEffect(() => {
        let apiUserIntervalId   

        if(token) {

            callApiUserData(token)

            apiUserIntervalId = setInterval(() =>{
                callApiUserData(token)
            },2000)

            
        }else{
            navigate("/")
        }

        return ()=>{
            if(apiUserIntervalId){
                clearInterval(apiUserIntervalId)
            }
        }


    },[])

    const callApiUserData = (token) => {

        axios.post(`${EnvConfig.BASE_URL}/view/user/${userId}`,null,{

            headers:{
                Authorization: `Bearer ${token}`
            },

            validateStatus: function (status) {
                return status == 200 || status == 201 || status == 400 || status == 401 || status == 500 || status == 404 || status == 501
            }

        }).then((response) => {
            
            if(response.status == 200) {
                console.log("200")
                console.log(response.data)
                setUserData(response.data.message)
                dispatch(set_initial_followers_value(response.data.message.totalfollwers))
            }else if(response.status == 404){
                console.log("400")
                console.log(response.data)
            }

            setIsLoading(false)

        }).catch((error) => {
            console.log("Some Error Occured...")
            console.log(error)
            setIsLoading(false)
        })

    }

    const followUser = async () => {
        const token = localStorage.getItem('my_token')

        if(token){
            const requestResult = await axios.patch(`${EnvConfig.BASE_URL}/follow/${userId}`,null,{

                headers:{
                    Authorization: `Bearer ${token}`
                },

                validateStatus: function (status) {
                    return status == 200 || status == 201 || status == 400 || status == 401 || status == 500 || status == 404 || status == 501
                }

            })

            console.log("Follow Response Data : --")
            console.log(requestResult.data)

            if(requestResult.status == 200) {
                dispatch(increment_followers_by_one())
            }

            // callApiUserData(token)

            setFollowUnfollowMsg(requestResult.data.message)

            setTimeout(()=>{
                setFollowUnfollowMsg("")
            },5000)
        }
    }


    const unFollowUser = async() => {
        const token = localStorage.getItem('my_token')

        if(token){

            const requestResult = await axios.patch(`${EnvConfig.BASE_URL}/unfollow/${userId}`,null,{

                headers:{
                    Authorization: `Bearer ${token}`
                },

                validateStatus: function (status) {
                    return status == 200 || status == 201 || status == 400 || status == 401 || status == 500 || status == 404 || status == 501
                }

            })

            console.log("Unfollow Response Data : --")
            console.log(requestResult.data)

            setFollowUnfollowMsg(requestResult.data.message)

            if(requestResult.status == 200) {
                dispatch(decrement_followers_by_one())
            }

            // callApiUserData(token)

            setTimeout(()=>{
                setFollowUnfollowMsg("")
            },5000)

        }
    }

    console.log("User id is : " + userId);


    return (
        <div>
            {
                isLoading ? (
                    <h1>Loading ...</h1>
                ):(
                    <div>

                        {
                            <div>

                                {<ViewStatus isLoggedIn={userData.isLoggedIn} textRequire={true}/>}

                                <h3>{userData.name}</h3>
                                <h3>Followers : {currFollowers}</h3>
                                <h3>Followings : {userData.totalfollowings}</h3>
                                <h3>Is Private Account : {JSON.stringify(userData.isPrivateAccount)}</h3>

                            </div>

                        }

                    </div>
                )
            }

            <br/>

            <button onClick={followUser}>Follow</button> &nbsp;
            <button onClick={unFollowUser}>UnFollow</button>

            <div>{followUnfollowMsg}</div>


        </div>
    )
}


export default TestElement;