import React,{useEffect,useState} from "react"
import {io} from 'socket.io-client';
import MainUtils from './FormUtils/MainUtils';
import '../../css/Home.css'
import EnvConfig from "../../config/config";
import {UnauthenticatedNavigations,AuthenticatedNavigations} from '../../Routers/navigations';



const Home = ()=>{

    const [streamData, setStreamData] = useState([]);
    const [cpuInfo,setCpuInfo] = useState([]);
    const [memoryInfo,setMemoryInfo] = useState([]);
    const [isConnected,setIsConnected] = useState(false);
    const maxLines = 16;

    useEffect(()=>{

        const socket = io(`http://${EnvConfig.BACKEND_IP}:${EnvConfig.BACKEND_PORT}`);

        socket.on('streamData', (data) => {
            // Update the state with the new data
            setStreamData((prevData) => {
                // Remove older lines if the box is full
                if (prevData.length >= maxLines) {
                // return [...prevData.slice(1), data];
                return []
                } else {
                return [...prevData, data];
                }
            });
        });

        setInterval(()=>{

            socket.emit("cpuInfo")

        },10000)

        setInterval(()=>{

            socket.emit("memoryInfo")

        },5000)

        socket.on("cpuInfo",(data)=>{

            setCpuInfo((prevData)=>{
                if(prevData.length >= maxLines){
                    return [];
                }else{
                    return[...prevData, data];
                }
            })
        })

        socket.on("memoryInfo",(data)=>{

            setMemoryInfo((prevData)=>{
                if(prevData.length >= maxLines){
                    return [];
                }else{
                    return[...prevData, data];
                }
            })
        })

        socket.on('connect', () => {
            console.log('Connect')
            setIsConnected(true); // Set isConnected to true when the socket connects
        });

        socket.on('disconnect', () => {
            console.log('Disconnect')
            setStreamData([])
            setCpuInfo([])
            setMemoryInfo([])
            setIsConnected(false); // Set isConnected to false when the socket disconnects
        });

        console.log('is_active conn')
        console.log(isConnected)

        return () => {
            socket.disconnect();
        };

    },[])

    return(
        <div>
            <br/>
            <div className="header-text">Home Page</div>
            <br/>
            <div>
                {
                    isConnected  ? (
                        <MainUtils pcData={streamData} cpuData={cpuInfo} memoryData={memoryInfo}/>
                    ):(
                        <div className="backend_down">Backend is Not Connected</div>
                    )
                }
            </div>
        </div>
    )
}



export default Home;