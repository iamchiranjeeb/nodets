import axios from "axios";
import EnvConfig from '../../../config/config'


const followingViewersData = async (token) =>{

    const followingDataResult = await axios.get(`${EnvConfig.BASE_URL}/view/followings`,{

        headers:{
            Authorization: `Bearer ${token}`
        },

        validateStatus: function (status) {
            return status === 200 || status === 201 || status === 400 || status === 401 || status === 500 || status === 404 || status === 501
        }

    })

    return followingDataResult
    
}

const followersData = async (token) =>{

    const followersData = await axios.get(`${EnvConfig.BASE_URL}/view/followers`,{

        headers:{
            Authorization: `Bearer ${token}`
        },

        validateStatus: function (status) {
            return status === 200 || status === 201 || status === 400 || status === 401 || status === 500 || status === 404 || status === 501
        }

    })

    return followersData
    
}

const getDashboardDatas = async(token) => {

    const dashboardResponse = await axios.get(`${EnvConfig.BASE_URL}/me`,{

        headers:{
            Authorization: `Bearer ${token}`
        },

        validateStatus: function (status) {
            return status === 200 || status === 201 || status === 400 || status === 401 || status === 500 || status === 404 || status === 501
        }

    })

    return dashboardResponse

}


const getPosts = async()=>{

    const token = localStorage.getItem('my_token')

    if(token){

        const allPosts = await axios.get(`${EnvConfig.BASE_URL}/view/blogs`,{

            headers:{
                Authorization: `Bearer ${token}`
            },

            validateStatus: function (status) {
                return status === 200 || status === 201 || status === 400 || status === 401 || status === 500 || status === 404 || status === 501
            }

        })

        return allPosts.data.blogs
    }

    return []

}


const deleteSinglePost = async(blog_id)=>{

    const token = localStorage.getItem('my_token')

    console.log('Dlt Token')
    console.log(token)

    if(token){

        const deletePost = await axios.delete(`${EnvConfig.BASE_URL}/delete/blog/${blog_id}`,{
            headers:{
                Authorization: `Bearer ${token}`
            },

            validateStatus: function (status) {
                return status === 200 || status === 201 || status === 400 || status === 401 || status === 500 || status === 404 || status === 501
            }
        })

        return deletePost

    }

    return null

}

export {getPosts,getDashboardDatas,followingViewersData,deleteSinglePost,followersData}