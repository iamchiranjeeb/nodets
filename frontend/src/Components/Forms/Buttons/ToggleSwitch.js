import './ToggleSwitch.css'

function ToggleSwitch({initValue,onToggle}){

    return(
        <label className="toggle">

            <input type="checkbox" checked={initValue} onChange={onToggle}/>
            <span className="slider"/>

        </label>
    )
}

export default ToggleSwitch