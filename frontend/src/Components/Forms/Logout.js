import React,{useEffect,useState,useContext} from 'react';
import { Route, useNavigate } from "react-router-dom";
import axios from "axios";
import EnvConfig from '../../config/config'
import AuthContext from '../../contexts/Auth/AuthContext';


function Logout(){

    const navigate = useNavigate();

    const {setIsAuthenticated} = useContext(AuthContext);


    useEffect(()=>{

        const token = localStorage.getItem('my_token')
        if(token) {

            axios.get(`${EnvConfig.BASE_URL}/logout/multiple/me`,{

                headers:{
                    Authorization: `Bearer ${token}`
                },

                validateStatus: function (status) {
                    return status == 200 || status == 201 || status == 400 || status == 401 || status == 500 || status == 404 || status == 501
                }

            }).then(()=>{

                console.log("Clearing Tokens")

            }).catch((err)=>{
                console.log("Error occured while doing logout")
                console.log(err)
            })

            localStorage.removeItem('my_token')
            
        }else{
            localStorage.removeItem('my_token');
        }

        setIsAuthenticated(false)
        navigate('/')

    })

    return (
        <div>
            <h2>Logging Out..</h2>
        </div>
    )


}

export default Logout