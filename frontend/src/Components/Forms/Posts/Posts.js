import React,{useState,useRef} from "react";
import axios from "axios";
import EnvConfig from "../../../config/config";

const BlogForm = ()=>{

    const [postHeader,setPostHeader] = useState('')
    const [postDescription,setPostDescription] = useState('')
    const [files,setFiles] = useState(null)
    const errorMessageRef = useRef("")
    const fileRef = useRef();
    const resetFile = () => {    fileRef.current.value = "";  };

    const changePostHeader = (event)=>{
        setPostHeader(event.target.value)
    }

    const changePostDescription = (event)=>{
        setPostDescription(event.target.value)
    }

    const handleFileChange = (e) => {
        // Convert FileList to an array and update state
        const fileList = Array.from(e.target.files);
        setFiles(e.target.files);
    };

    const submitHandler = async(e) => {

        e.preventDefault()

        try{

            const token = localStorage.getItem('my_token')

            const formData = new FormData();
            formData.append('header',postHeader)
            formData.append('descrpition',postDescription)
            for(const file of files) {
                formData.append('file',file)
            }
           
           

            const response = await axios.post(`${EnvConfig.BASE_URL}/create/blog`,formData,{

                headers: {
                    Authorization: `Bearer ${token}`,
                },

                validateStatus: function(status) {
                    return status === 200 || status === 400 || status === 401 ||status === 500 || status === 404
                }

            })

            if(response.status === 200){
                setPostHeader('')
                setPostDescription('')
                setFiles(null)
                resetFile()
                alert('Blog Posted..')
            }else{
                e.preventDefault()
            }

        }catch(e){
            console.log("Error While doing login ")
            console.log(e)
            errorMessageRef.current.textContent = e.message
            alert(e.message)
        }

    }

    return (
        <div>

            <form onSubmit={submitHandler}>

                <h1>Post Something....</h1>
                
                <br/>
                <br/>

                <label>
                    Header :
                    <input type="text" value={postHeader} id="post-header" name="post-header" onChange={changePostHeader} />
                </label>

                <br/>
                <br/>

                <label>
                    Description :
                    <input type="text" value={postDescription} id="post-desc" name="post-desc" onChange={changePostDescription} />
                </label>

                <br/>
                <br/>

                <div style={{display:'flex',alignItems: 'center',justifyContent: 'center'}}>
                    <label>
                        Photos/Videos: 
                    </label>
                    <input type="file" ref={fileRef} multiple onChange={handleFileChange} />
                </div>
                
                <br/>
                <br/>

                <div>
                    <button type="submit">Submit</button>
                    {/* <button onClick={submitHandler}>Post Blog</button> */}
                </div>

                <p ref={errorMessageRef}></p>
                
            </form>

        </div>
    )

}

export default BlogForm