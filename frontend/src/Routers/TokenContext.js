import { createContext, useContext, useState,useEffect } from "react";
 
const defaultContextValue = {
  isAuthenticted: false,
};
 
const TokenContext = createContext(defaultContextValue);
 
const TokenProvide = ({ children }) => {
  const [isAuthenticated,setIsAuthenticated] = useState(false)

  useEffect(() => {
      const token = localStorage.getItem('my_token')
      console.log("Token is " + token)

      if(token) {
          setIsAuthenticated(true);
      }else{
          localStorage.removeItem('my_token');
          setIsAuthenticated(false);
      }

  },[])
 
  return (
    <TokenContext.Provider value={{ isAuthenticated,setIsAuthenticated }}>
      {children}
    </TokenContext.Provider>
  );
};
 
const useToken = () => useContext(TokenContext);
 
export { useToken, TokenProvide };