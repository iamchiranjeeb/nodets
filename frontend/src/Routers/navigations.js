import React from 'react';
import { NavLink } from 'react-router-dom';
import '../css/navigations.css'

const UnauthenticatedNavigations = () => (

  <header>

    <nav>
      <ul>

        <li> <NavLink to="/"><div className='textHomePage'>Home</div></NavLink> </li>
        <li> <NavLink to="/signup"><div className='textHomePage'>Signup</div></NavLink> </li>
        <li> <NavLink to="/login"><div className='textHomePage'>Login</div></NavLink> </li>
        {/* <li> <NavLink to="/video">CHeck Videos</NavLink> </li> */}

      </ul>
    </nav>

  </header>
);

const AuthenticatedNavigations = () => (
  <nav>
    <ul>

      <li> <NavLink to="/dashboard">Dashboard</NavLink> </li>
      <li> <NavLink to="/logout">Logout</NavLink> </li>

    </ul>
  </nav>
);

export {UnauthenticatedNavigations,AuthenticatedNavigations}
