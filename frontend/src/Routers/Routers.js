import React,{useState,useEffect,useContext} from "react";
import {UnauthenticatedNavigations,AuthenticatedNavigations} from "./navigations";
import { BrowserRouter as Router, Route, Routes,Navigate } from 'react-router-dom';
import Home from '../Components/Forms/Home';
import Signup from '../Components/Forms/Signup'
import Login from '../Components/Forms/Login';
import UserPage from '../Components/Forms/Users/UserPage';
import Dashboard from '../Components/Forms/Dashboard';
import Logout from '../Components/Forms/Logout';
import AuthContext from '../contexts/Auth/AuthContext';
import VideoPlayer from '../Components/Forms/VideoTest';


function AppRouters(){

    const {isAuthenticated} = useContext(AuthContext)

    return (
        <Router>

                {
                    isAuthenticated ? (
                        <>
                            <AuthenticatedNavigations/>
                            {/* <Routes>
                                <Route path="/dashboard" element={<Dashboard/>}/>
                                <Route path="/user/:userId" element={<UserPage />} />
                                <Route path="/logout" element={<Logout />} />
                            </Routes> */}
                        </>
                    ):(
                        <>
                            <UnauthenticatedNavigations/>
                            {/* <Routes>
                                <Route path="/signup" element={<Signup />} />
                                <Route path="/login" element={<Login />} />
                                <Route path="/" element={<Home />} />
                            </Routes> */}
                        </>
                    )
                }

                <Routes>
                    <Route path="/dashboard" element={<Dashboard/>}/>
                    <Route path="/user/:userId" element={<UserPage />} />
                    <Route path="/signup" element={<Signup />} />
                    <Route path="/login" element={<Login />} />
                    <Route path="/logout" element={<Logout />} />
                    <Route path="/" element={<Home />} />
                    <Route path="/video" element={<VideoPlayer/>}/>
                </Routes>
    

    
        </Router>
    )

  
}

export default AppRouters